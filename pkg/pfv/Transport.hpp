/*************************************************************************
*  Copyright (C) 2021 by Robert Caulk <rob.caulk@gmail.com>  		 *
*  Copyright (C) 2021 by Bruno Chareyre <bruno.chareyre@grenoble-inp.fr> *
*  Copyright (C) 2021 by Nadia Mokni					 *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/
/* This engine is under active development. Experimental only */
/* Transport Engine was developed with funding provided by the Institut de Radioprotection et la Surete Nucleaire*/

/* Theoretical framework and experimental validation presented in:

Caulk, R. Mokni, N., Chareyre, B. (To be submitted) A discrete element approach to modelling partially saturated nuclear waste seals comprised of clay pellet/powder mixtures.

*/

#define TRANSPORTFLOW
#ifdef TRANSPORTFLOW
#ifdef YADE_OPENMP
#pragma once

#include <core/PartialEngine.hpp>
#include <core/State.hpp>
#include <core/Dispatching.hpp>
#include <pkg/dem/JointedCohesiveFrictionalPM.hpp>
#include <pkg/dem/ScGeom.hpp>
#include <pkg/dem/HertzMindlin.hpp>
#include <pkg/common/ElastMat.hpp>

#ifdef FLOW_ENGINE
#include "FlowEngine_TransportFlowEngineT.hpp"
#include <lib/triangulation/FlowBoundingSphere.hpp>
#include <lib/triangulation/Network.hpp>
#include <lib/triangulation/Tesselation.h>
#include <pkg/dem/TesselationWrapper.hpp>
#endif

#ifdef YADE_VTK

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wsuggest-override"
//#include<vtkSmartPointer.h>
#include <lib/compatibility/VTKCompatibility.hpp>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkLine.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkQuad.h>
#include <vtkSmartPointer.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLUnstructuredGridWriter.h>
//#include<vtkDoubleArray.h>
#include<vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#pragma GCC diagnostic pop

#endif

namespace yade {

class TransportCellInfo : public FlowCellInfo_TransportFlowEngineT {
public:
	Real fluxSum;
	Real Keq;
	Real dpds;
	Real potential;
	Real scalarH;
	Real diffusionD;
	Real conductivity;
	bool Pcondition;
	Real oldPotential;
	Real saturation;
	Real initialSaturation;
	Real initialPorosity;
	Real Po;
	Real lambdao;
	Real porosity;
	Real vWater; // volume of water only
	Real vSolids; // volume of solids only (no water or air)
	Real vMixture; //volume solids + water + air in powder (does not include void space of cell)
	Real vMixtureOriginal;
	Real equivalentBulkModulus;
	Real massWater;  // mass wetting phase
	Real massSolids; // mass solid phase
	Real oldMassWater;
	Real swellingPressure;
	Real swellingPressure0;
	Real initialDryDensity;
	bool filled; // vMixture > invVoidVolume()
	Real vMixtureChange;
	Real stabilityCoefficient;
	Real vStrain;
	Real isCell;
	//Real vMixture; // volume solids + water + air
	TransportCellInfo(void)
	{
	vStrain=stabilityCoefficient=vWater=vSolids=vMixture=vMixtureOriginal=vMixtureChange=initialDryDensity=fluxSum=Keq=dpds=potential=oldPotential=swellingPressure=swellingPressure0=scalarH=diffusionD=conductivity = massWater= oldMassWater =saturation=initialSaturation=Po=lambdao=porosity=initialPorosity=equivalentBulkModulus=0;
	filled=Pcondition=false;
	isCell=true;
	}
};

class TransportVertexInfo : public FlowVertexInfo_TransportFlowEngineT {
public:
	Real fluxSum; // net sum of fluxes for the particle
	Real Keq; //
	Real potential; // this term controls the gradients of movement, for partial sat this is pressure
	Real oldPotential;
	Real conductivity; // diffusion term, for partial sat this is the permeability. this is the upscaled perm model
	Real dpds; // can we name this something more generic? Wrap this into diffusion D?
	Real scalarH; // transported quantity, for partial sat this is saturation
	bool Pcondition;
	Real diffusionD;
	Real lambdao;
	Real Po;
	Real vMixtureOriginal;
	Real vMixture; //volume;
	Real radiiOriginal;
	Real radiiChange;
	Real saturation;
	Real initialPorosity;
	Real initialSaturation;
	Real porosity;
	Real equivalentBulkModulus;
	Real vSolids;
	Real massWater;
	Real oldMassWater;
	Real youngs;
	Real stabilityCoefficient;
	Real vStrain;
	Real isCell;
	TransportVertexInfo(void)
	{
	vStrain=stabilityCoefficient=youngs=fluxSum=Keq=potential=oldPotential=diffusionD=scalarH=dpds=conductivity=lambdao=Po=vSolids=vMixture=vMixtureOriginal=radiiOriginal=radiiChange=saturation=porosity=equivalentBulkModulus=initialPorosity=initialSaturation=oldMassWater=massWater;
	Pcondition=false;
	isCell=false;
	}

	inline const Real& invVoidVolume (void) const {return 0;} // dummy just to satisfy auto type arguments.

};

typedef TemplateFlowEngine_TransportFlowEngineT<TransportCellInfo, TransportVertexInfo> TransportFlowEngineT;
REGISTER_SERIALIZABLE(TransportFlowEngineT);

class TransportFlowEngine : public TransportFlowEngineT {
public:
	//typedef TemplateFlowEngine_TransportFlowEngineT<FlowCellInfo_TransportFlowEngineT, FlowVertexInfo_TransportFlowEngineT> TransportFlowEngineT;
	typedef TransportFlowEngineT::Tesselation                                                             Tesselation;
	typedef TransportFlowEngineT::RTriangulation                                                          RTriangulation;
	typedef TransportFlowEngineT::FiniteCellsIterator                                                     FiniteCellsIterator;
	typedef RTriangulation::Finite_facets_iterator                                               	      FiniteFacetsIterator;
	typedef TransportFlowEngineT::CellHandle                                                              CellHandle;
	typedef TransportFlowEngineT::VertexHandle                                                            VertexHandle;
	typedef TransportFlowEngineT::Facet                                                                   Facet;
	typedef std::vector<CellHandle>                                                                       VectorCell;
	typedef typename VectorCell::iterator                                                                 VCellIterator;
	typedef Eigen::Matrix<Real, 2, 2> 									EMatrix2r;
	typedef Eigen::Matrix<Real, 2, 1> 									EVector2r;

public:

//#define parallel_forces
// #ifdef parallel_forces
// 	int                            ompThreads;
// 	vector<vector<const CVector*>> perVertexUnitForce;
// 	vector<vector<const Real*>>    perVertexPressure;
// #endif
	//

	virtual ~TransportFlowEngine();
	virtual void action() override;
	virtual void buildTriangulation(Real pZero, Solver& flow) override;
	void 	     applySwellingForces(Solver& flow);
	void 	     boundaryConditions(Solver& flow);
	void         setInitialValues();
	void         resetFlowBoundaryTemps();
	void         resetBoundaryFluxSums();
	void         setParticleTransferBoundary();
	void         transportExpansion();
	void         initializeInternalEnergy();
	void         computeNewCellPotentials();
	void         computeNewParticlePotentials();
	void         computeParticleCellTransfer();
	void         computeCellCellTransfer();
	void         updateForces();
	void         computeVertexSphericalArea();
	void         computeFlux(CellHandle& cell, const shared_ptr<Body>& b, const Real surfaceArea);
	void         computeSolidSolidFluxes();
	void         timeStepEstimate();
	CVector      cellBarycenter(const CellHandle& cell);
	void         computeCellVolumeChangeFromDeltaTemp(CellHandle& cell, Real cavDens);
	void         accountForCavitySolidVolumeChange();
	void         accountForCavityTransportVolumeChange();
	void         unboundCavityParticles();
	void         computeCellVolumeChangeFromSolidVolumeChange(CellHandle& cell);
	void 	     computeEquivalentBulkModuli(FlowSolver& flow);
	Real         getFirst() const { return first; }
	Real 	     getCellPotential(Vector3r pos);
	Real 	     getVertexPotential(const int id);
	void         setVerticesConductivities();
	void         setCellConductivities();
	void 	     computeParticleParticleTransfer();
	void 	     setNewParticlePotentials();
	void 	     setNewCellPotentials();
  	Real 	     getCellConductivity(const CellHandle& cell);
	Real 	     getVertexConductivity(const VertexHandle& Vh);
        Real         computeParticleParticleDiffusion(const VertexHandle& Vh1, const VertexHandle& Vh2, const shared_ptr<Body>& b1, const shared_ptr<Body>& b2);
	Real 	     computeCellCellDiffusion(const CellHandle& cell1, const CellHandle& cell2, const int j);
	void         initializePotential();
	void         initializeSaturations(Solver& flow);
	void 	     computeParticleCellFlux(CellHandle& cell, const VertexHandle& v, const shared_ptr<Body>& b, const Real surfaceArea);
	Real 	     computeParticleCellDiffusion(const VertexHandle& Vh1, const CellHandle& Cell2, const shared_ptr<Body>& b1, const Real surfaceArea);
	void 	     computeCellCellFluxes();
	void 	     setCellsDPDS(Solver& flow);
	void 	     setVerticesDPDS(Solver& flow);
	void 	     setInitialPorosity(FlowSolver& flow);
	void 	     setInitialMixtureVolumes(FlowSolver& flow);
	Real         weibullDeviate(Real lambda, Real k);
	void 	     setSaturationFromPcS(CellHandle& cell);
	Real         dsdp(CellHandle& cell);
	Real         dsdp(const VertexHandle& Vh);
	Real         dpds(CellHandle& cell);
	Real         dpds(const VertexHandle& Vh);
	Real	     vanGenuchten(CellHandle& cell, Real pc);
	Real	     vanGenuchten(const VertexHandle& Vh, Real pc);
	Real	     vanGenuchten_pressure(CellHandle& cell, Real s);
	Real	     vanGenuchten_pressure(const VertexHandle& Vh, Real s);
	Real	     vanGenuchten_pressure_general(auto system, Real s);
	void 	     updatePorosity(FlowSolver& flow);
	void 	     resetFlowBoundaryPotentials();
	void         particleVolumetricChange();
	Real 	     interpolate( vector<Real> &xData, vector<Real> &yData, Real x, bool extrapolate );
	void 	     readConductivityFile();
	void 	     updateSaturation(FlowSolver& flow);
	void 	     interpolateTesselations(Tesselation& Tes, Tesselation& NewTes, Solver& flow);
	void 	     interpolateVertices(Tesselation& Tes, Tesselation& NewTes);
	void 	     computeFacetForcesWithCache(bool onlyCache);
	void 	     computeSwellingPressure();
	void 	     cellVolumetricChange();
	Real 	     getMaxSwellingPressure(CellHandle& cell);
	void 	     updateParticleYoungsModulus();
	void 	     computeFacetSurfaces();
	Real 	     findMaxTimestep();
	void         setVolumeFractionWithImageryGrid(string imageryFilePath2, FlowSolver& flow);
	void 	     averageRelativeCellVelocity_transport();
	void 	     minimizeInconsistencies(CellHandle& cell);
	void 	     minimizeInconsistencies(const VertexHandle& Vh, Sphere* sphere);
	Real 	     volume_from_pressure(CellHandle& cell, const Real pressure);
	Real 	     volume_from_pressure(const VertexHandle& Vh, const Real pressure);
	void 	     solveWithNewtonRaphson(auto system);
	Real 	     functionVanGenuchten(const auto system, const Real s,const Real V,const Real p);
	Real 	     functionSaturation(const auto system, const Real s,const Real V,const Real p);
	Real 	     functionVolume(const auto system, const Real s,const Real V,const Real p);
	Real 	     function1(const auto system,const Real V,const Real p, const Real mu);
	Real 	     function2(const auto system,const Real V,const Real p, const Real mu);
	Real	     dfunction1dv(const auto system, const Real V);
	Real	     dfunction2dp(const auto system, const Real p);
	void 			 updateFacetSurfaces();

	//class VertexHandle;
	// CellHandle findNearestVertex(Tesselation& Tes, CVector newPoint);
#ifdef YADE_VTK
	void 	     saveVertexInfoVTK(string fileName);
	void 	     saveCellInfoVTK(const char* folder, bool withBoundaries);
#endif
	std::vector<Real>     suctions;
	std::vector<Real>     conductivities;
	bool 		      useCondTextFile=false;
	Real 		      oneOverRhoTimesGravity = 0;
	int 	              elapsedIters = 0;
	Real         maxDSDPj = 0;
	//void         applyBoundaryHeatFluxes();
	// clang-format off
		YADE_CLASS_BASE_DOC_ATTRS_INIT_CTOR_PY(TransportFlowEngine,TransportFlowEngineT,"An generalized mass transport engine that can be used for simulating any analog (species, heat, etc.) of transport between particles, between cells, and between particles and cells",
		/*attributes*/
		((Real,lmbda,0.08,,"Lambda parameter for Van Genuchten model. Free swelling 0.4. If porosity is distributed, this value becomes cell based."))
		((Real,Po,0.03e6,,"Po parameter for Van Genuchten model, Free swelling 0.04e6. If porosity is distributed, this value becomes cell based."))
		((Real,a,6.8,,"parameter a for evolution of Po as a function of porosity"))
		((Real,b,-1.5,,"parameter b for evolution of lambda as a function of porosity"))
		((Real,Bfail,0,,"Coefficient for the pellet failure exponential equation."))
		((Real,Ca,1.2e-7,,"Coefficient for the pellet failure, converts R strength to force. Darde 2021."))
		((bool,advection,true,,"Activates advection"))
		((bool,cellTransfer,true,,"Activates transfer between cells"))
		((bool,debug,false,,"debugging flags"))
		((bool,debug_precise,false,,"debugging flags, precise"))
		((bool,particleTransfer,true,,"Activates transfer between particles"))
		((bool,particleCellTransfer,true,,"Activates transfer between particles and cells"))
		((bool,cellCellTransfer,true,,"Activates transfer between cells"))
		((bool,particleExpansion,true,,"Activatesvolumetric expansion of particles"))
        	((bool,cellExpansion,true,,"Activates volumetric expansion of cells"))
		((Real,alpham,0.024e-06,,"alpha parameter for particle volumetric strain model MPa^-1 Darde 2021"))
		((Real,betas,6.75,,"beta parameter for swelling pressure model. Wang 2012"))
		((Real,alphas,1.78e-04,,"alpha parameter for swelling pressure model. Wang 2012"))
		((Real,betam,0.016e-6,,"beta parameter for particle volumetric strain model MPa^-1. Darde 2021"))
        	((bool,solidThermoMech,true,,"Activates thermoMech"))
		((bool,ignoreFictiousTransfer,false,,"Allows user to ignore transfer between fictious cells and boundary particles. Mainly for debugging purposes."))
		((vector<bool>, particleBndCondIsDirichlet, vector<bool>(6,false),,"defines the type of boundary condition for each side. True if value is constant and imposed, False for no heat-flux. Indices can be retrieved with :yref:`FlowEngine::xmin` and friends."))
		((vector<Real>, particleTransferBndCondValue, vector<Real>(6,0),,"Imposed value of a dirichlet boundary condition."))
		((vector<Real>, particleTransferBndFlux, vector<Real>(6,0),,"Flux through transport boundary."))
		((vector<Real>, cellTransferBndCondValue, vector<Real>(6,0),,"Imposed value of a dirichlet boundary condition."))
		((vector<Real>, cellBndCondIsDirichlet, vector<Real>(6,0),,"defines if boundaries of cell network are dirichlet or not"))
		((bool,boundarySet,false,,"set false to change boundary conditions"))
		((bool,useKernMethod,false,,"flag to use Kern method for transport conductivity area calc"))
		((bool,useHertzMethod,false,,"flag to use hertzmethod for transport conductivity area calc"))
        	((Real,cellBeta,0.0002,,"volumetric expansion coefficient for cell, default 0.0002, <= 0 deactivates"))
        	((Real,particleP0,0,,"Initial potential of particles"))
		//((bool,useVolumeChange,false,,"Use volume change for transport-mechanical-hydraulic coupling instead of pressure change. False by default."))
		((bool,letTransportRunFlowForceUpdates,false,,"If true, Transport will run force updates according to new pressures instead of FlowEngine. only useful if useVolumeChange=false."))
		((bool,flowTempBoundarySet,true,,"set false to change boundary conditions"))
		((bool,unboundCavityBodies,true,,"automatically unbound bodies touching only cavity cells."))
		((Real,particleConductivity,3.0e-12,,"Particle transport conductivity, m/s (L/T)"))
		((Real,cellConductivity,3.0e-11,,"cell transport conductivity, m/s (L/T)"))
		((Real,particleCp,750.,,"Particle transport storage capacity "))
		((Real,cellTransferAreaFactor,1.,,"Factor for the porethroat area (used for fluid-fluid conduction model)"))
		((Real,particleAlpha,11.6e-6,,"Particle volumetric transport expansion coeffcient"))
		((Real,particleDensity,0,,"If > 0, this value will override material density for thermodynamic calculations (useful for quasi-static simulations involving unphysical particle densities)"))
        	((Real,fluidK,0.580,,"Transport conductivity of the fluid."))
		((Real,uniformReynolds,-1.,,"Control reynolds number in all cells (mostly debugging purposes). "))
		((Real,cellBulkModulus,0,,"If > 0, transportEngine uses this value instead of flow.fluidBulkModulus."))
		((Real,delT, 0,,"Allows user to apply a delT to solids and observe macro transport expansion. Resets to 0 after one conduction step."))
        	((Real,tsSafetyFactor,1,,"Allow user to control the timstep estimate with a safety factor. Default 0.8. If <= 0, transport timestep is equal to DEM"))
        	((Real,porosityFactor,0,,"If >0, factors the fluid transport expansion. Useful for simulating low porosity matrices."))
        	((bool,tempDependentFluidBeta,false,,"If true, fluid volumetric transport expansion coefficient, :yref:`TransportFlowEngine::fluidBeta`, is temperature dependent (linear model between 20-70 degC)"))
        	((Real,minimumFluidCondDist,0,,"Useful for maintaining stability despite poor external triangulations involving flat tetrahedrals. Consider setting to minimum particle diameter to keep scale."))
		((Real,cellP0,0,,"Initial cell pressure (suction) "))
		((bool,freezeSaturation,0,,"Don't change saturation"))
		((Real,pAir,0,,"Air pressure for calculation of capillary pressure (Pair - Pwater)"))
		((Real,Ka,101e3,,"bulk modulus of air used for equivalent compressibility model"))
		((Real,Kw,2.15e9,,"bulkmodulus of water used for equivalent compressibility model"))
		((Real,Ks,21.2e9,,"bulkmodulus of solid used for equivalent compressibility model"))
		((Real,fluidBeta,0,,"Air pressure for calculation of capillary pressure (Pair - Pwater)"))
		((Real,SrM,0,,"residual saturation for empirical relative saturation based permeability relationship"))
		((Real,SsM,1.,,"saturated saturation for empirical relative saturation based permeability relationship"))
		((Real,initialSaturation,0.5,,"initial constant saturation for domain"))
		((Real,particleParticleDiffusionCoefficient,1,,"factoring the particle diffusion calculation"))
		((Real,particleCellDiffusionCoefficient,1,,"factoring the particle diffusion calculation"))
		((Real,cellCellDiffusionCoefficient,1,,"factoring the particle diffusion calculation"))
		((Real,totalVolChange,0,,"tracks the total volumetric strain that occured in each step"))
		// ((bool,useMicroScaleData,false,,"tracks the total volumetric strain that occured in each step"))
		((string,conductivityTextFile,"none",,"path to the microscopic conductivity data. Columns suction (pa) conductivtiy (m/s)"))
		((Real,rho,1000,,"Density of the wetting phase."))
		((Real,rho_solid,2700,,"Density of the solid phase."))
		((Real,gravity,9.81,,"Acceleration due to gravity, used for conductivity conversion"))
		((Real,meanInitialPorosity,-1,, " if not negative, activates stochastic distribution for porosity. mean value of porosity for specimen"))
		((Real,maxPorosity,-1,, "max porosity found during stochastic poro distribution. used for evolution of porosity"))
		((Real,lambdaWeibullShape,6.,,"shape of weibull distribution of the correction factor used for porosity distribution."))
		((Real,kappaWeibullScale,1.,, "scale of weibull dist, this is the mean correction factor multiplied by :yref:`meanInitialPorosity<TransportFlowEngine.meanInitialPorosity>`"))
		((bool,homogeneousPorosity,true,,"use the meanInitialPorosity everywhere instead of random distribution"))
		((bool,resetVolumeSolids,false,,"useful if genesis process was used to reach an initial condition. We don't want the volume changes that occured during geneis to affect porosity evolution."))
		((Real,minPoroClamp,0.1,,"min clamp for distribution of porosity"))
		((Real,maxPoroClamp,0.6,,"max clamp for distribution of porosity. Value over 0.8 messes with water retention curve"))
		((Real,minClampSaturation,0.05,,"min clamp for distribution of porosity"))
		((Real,maxClampSaturation,0.99,,"max clamp for distribution of porosity. Value over 0.8 messes with water retention curve"))
		((bool,freezePorosity,false,,"useful for freezing porosity values during stage for reaching initial conditions where volume changes should not impact porosity"))
		((bool,transportEngine,true,,"activates interpolation function."))
		((Real,avgParticleDiffusion,0,,"average conductivity"))
		((Real,avgCellDiffusion,0,,"average conductivity"))
		((int,numCellDiffusion,0,,"count of cell diffusions"))
		((int,numParticleDiffusion,0,,"count of particle diffusions"))
		((bool,swellingPressure,true,,"activate cell swelling pressure."))
		((Real,conductivityFactor,1,,"Multiply the values in the conductivity text file by this value (useful for speeding up simulation. equivalent to modifying viscosity)"))
		((bool,setParticlePorosity,false,,"Set the particle porosity using a weibuldeviate"))
		((bool,useEquivalentCompressibility,false,,"Compute and include the equivalent compressibility of the vertices and cells."))
		((Real,initialDryDensity,1.9,,"Set the initial dry density of the cells."))
		((Real,initialCellMixtureFraction,0.5,,"Set the initial mixture cell volume fraction."))
		((bool,homogeneousMixtureVolumes,false,,"Distribute mixture volumes stochastically according to weibull params."))
		((Real,swellingPressureFactor,1,,"Factor the swelling pressure, mostly for debugging purposes."))
		((Real,checkStability,0,,"If > 0, check stability and automatically change timestep every N iterations. Else use scene->dt"))
		((Real,minYoung,100e6,,"Limit the minimum possible young's modulus in conjunction with youngsModulusChange.Darde 2018"))
		((Real,maxYoung,800e6,,"Limit the maximum possible young's modulus in conjunction with youngsModulusChange. Darde 2018"))
		((Real,lambda_relax,0.25,,"lambda coefficient for the exponential decay function of the relaxation factor of newton raphson."))
		((bool,useNewtonRaphson,false,,"Use newton raphson to solve the saturation, pressure, volume of each system simultaneously."))
		((Real,errorThreshold,0.01,,"RMS error required for ending newton raphson."))
		((Real,hfactor,0.0001,,"Factor the h value by this number."))
		((Real,mu_start,0.8,,"If constrain_newton True, use this value to factor the constraint function."))
		((bool,constrain_newton,false,,"Constrain volume in newton."))
		((bool,newtonUseLdlt,false,,"Use ldlt decomposition for newton."))
		((bool,waitForUnbalanced,false,,"Skip computing fluxes and new V, P, to let user equilibrate forces before initiaing hydration."))
		((bool,onlyUpdateFacetSurfaces,false,,"Avoid expensive retriangulation by only updating geometric quantities."))
		((bool,youngsModulusChange,false,,"Young's modulus of pellets changes wrt suction. Plasticity computed according to exponential. Darde 2021."))
		((Real,iterativeSubStepFactor,0,,"If >0, the new cell volumes and saturations will be set simultaneously through an iterative substep. The error will be below this value."))
		((Real,maxSuctionConstraint,132e6,,"If >0, the new cell volumes and saturations will be set simultaneously through an iterative substep. The error will be below this value."))
		((Real,minSuctionConstraint,10e3,,"If >0, the new cell volumes and saturations will be set simultaneously through an iterative substep. The error will be below this value."))
		((Real,minimumLength,0,,"if distance between two cells/particles is smaller than this value, replaceit with thisvalue. Good for incident tetrahedra with shared cell centers"))
		((Real,allowableFacetSphereRatioForFilled,0.01,,"Even if vMixture > voidVolume(), swelling pressure not considered until facetsphere ratio falls below this value. This is protecting against very open cells where powder would escape through large openings to neighbor. This is an average of facet fluid ratios for all 4 facets comprising the cell."))
		,
		/* extra initializers */
		,
		/* ctor */
		solver = shared_ptr<FlowSolver> (new FlowSolver);
		,
		/* py */
        	/* .def("getTransportDT",&TransportFlowEngine::getTransportDT,"let user check estimated thermalDT .") */
        	/* .def("getParticleTransferIterPeriod",&TransportFlowEngine::getParticleTransferIterPeriod,"let user check estimated conductionIterPeriod .") */
        	.def("getCellPotential",&TransportFlowEngine::getCellPotential,(boost::python::arg("pos")),"Get cell potential using coordinates of a point.")
 		.def("getVertexPotential",&TransportFlowEngine::getVertexPotential,(boost::python::arg("id")),"Get vertex potential given the id of the body of interest.")
		.def("saveVertexInfoVTK",&TransportFlowEngine::saveVertexInfoVTK,(boost::python::arg("fileName")="./VTK"),"Save vertices")
		.def("saveCellInfoVTK",&TransportFlowEngine::saveCellInfoVTK,(boost::python::arg("folder")="./VTK",boost::python::arg("withBoundaries")=false),"Save pressure and saturation field in vtk format. Specify a folder name for output. The cells adjacent to the bounding spheres are generated conditionally based on :yref:`withBoundaries`")
	)
	// clang-format on
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(TransportFlowEngine);

} // namespace yade

#endif //TRANSPORT
#endif //YADE_OPENMP
