/*************************************************************************
*  Copyright (C) 2021 by Robert Caulk <rob.caulk@gmail.com>  		 *
*  Copyright (C) 2021 by Bruno Chareyre <bruno.chareyre@grenoble-inp.fr>     *
*  Copyright (C) 2021 by Nadia Mokni									 *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/
/* This engine is under active development. Experimental only */
/* Transport Engine was developed with funding provided by the Institut de Radioprotection et la Surete Nucleaire*/

/* Theoretical framework and experimental validation presented in:

Caulk, R. Mokni, N., Chareyre, B. (To be submitted) A discrete element approach to modelling partially saturated nuclear waste seals comprised of clay pellet/powder mixtures.

*/

#define TRANSPORTFLOW
#ifdef TRANSPORTFLOW
#ifdef YADE_OPENMP
#include "Transport.hpp"
#include <core/Omega.hpp>
#include <core/Scene.hpp>
#include <pkg/common/Sphere.hpp>
#include <pkg/dem/FrictPhys.hpp>
#include <pkg/dem/Shop.hpp>
#include <pkg/pfv/Transport.hpp>

namespace yade {

CREATE_LOGGER(TransportFlowEngine);
YADE_PLUGIN((TransportFlowEngine)(TransportFlowEngineT));

TransportFlowEngine::~TransportFlowEngine() { } // destructor

void TransportFlowEngine::action()
{
	//if (!first and elapsedIters < transportIterPeriod) return; // dont do anything
	if (debug) cout << "initial values set" << endl;
	setPositionsBuffer(true);
        if (first) {
		buildTriangulation(pZero,*solver);
		if (debug) cout << "built tri" << endl;
		//initializeSaturations(*solver);
		if (debug) cout << "initialized saturations" << endl;
		initializeVolumes(*solver);
		readConductivityFile();
		setInitialMixtureVolumes(*solver);
		setInitialPorosity(*solver);
		setVerticesConductivities();
		if (debug) cout << "set conductivities" << endl;
		setCellConductivities();
	}

        timingDeltas->checkpoint ( "Triangulating" );
	updateVolumes ( *solver );
	if (debug) cout << "updated volumes" << endl;
        timingDeltas->checkpoint ( "Update_Volumes" );

        epsVolCumulative += epsVolMax;
	retriangulationLastIter++;
	if (!updateTriangulation) updateTriangulation = // If not already set true by another function or by the user, check conditions
		(defTolerance>0 && epsVolCumulative > defTolerance) || (meshUpdateInterval>0 && retriangulationLastIter>=meshUpdateInterval);

	// update the diffusion terms of the particle and cell network
	// setVerticesConductivities();
	// if (debug) cout << "set conductivities" << endl;
	// setCellConductivities();

	if (!waitForUnbalanced){
		if (!first and !freezePorosity and iterativeSubStepFactor==0 and !useNewtonRaphson) {
			updatePorosity(*solver);
		}

		if (debug) cout << "boundaries set" << endl;

		numCellDiffusion = 0; avgCellDiffusion = 0;
		numParticleDiffusion = 0; avgParticleDiffusion = 0;

	// compute the fluxes

		if (particleTransfer) computeParticleParticleTransfer();
		if (particleCellTransfer) computeParticleCellTransfer();
		if (cellCellTransfer) computeCellCellFluxes();

		avgCellDiffusion = avgCellDiffusion/numCellDiffusion;
		avgParticleDiffusion = avgParticleDiffusion/numParticleDiffusion;

	// integrate through time

		if (debug) cout << "particleTransfer done" << endl;
		if (particleTransfer) setNewParticlePotentials();
		if (cellCellTransfer or particleCellTransfer) setNewCellPotentials();
		if (debug) cout << "potentials set" << endl;

		// swell particles and powder
		if (particleExpansion and iterativeSubStepFactor==0 and !useNewtonRaphson) particleVolumetricChange();
		if (cellExpansion and iterativeSubStepFactor==0 and !useNewtonRaphson) cellVolumetricChange();
	}

	if (swellingPressure) computeSwellingPressure();

	// set new particle youngs modulus
	if (youngsModulusChange and !waitForUnbalanced) updateParticleYoungsModulus();

	// apply forces for newton
	if (letTransportRunFlowForceUpdates) updateForces();

	// detect max timestep and adjust accordingly
	if ((checkStability > 0 ) and elapsedIters == checkStability) {
		setCellsDPDS(*solver);
		setVerticesDPDS(*solver);
		scene->dt = tsSafetyFactor * findMaxTimestep();
		//if (debug_precise) cout << "found max timestep " << scene->dt << endl;
		// transportIterPeriod = int(transportTS / scene->dt);
		elapsedIters = 0;
	}

	if (checkStability > 0) elapsedIters += 1;

	// rebuild triangulation
  if (updateTriangulation && !first) {
		if (onlyUpdateFacetSurfaces) updateFacetSurfaces(); // avoid expense and possibile discontinuities associated with mesh interpolation etc.
		else {
			buildTriangulation (pZero, *solver);
			initializeVolumes(*solver);
		}

		setVerticesConductivities();
		if (debug) cout << "set conductivities" << endl;
		setCellConductivities();

		retriangulationLastIter=0;
		updateTriangulation=false;
		ReTrg++;}
	first = false;
}

Real TransportFlowEngine::findMaxTimestep()
{
	Tesselation& Tes = solver->T[solver->currentTes];
	Real maxTimeStep = 1;
	FOREACH ( const shared_ptr<Body>& b, *scene->bodies ) {
		if (!b) continue;
		const Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh1 = Tes.vertex(b->getId());
		if (Vh1->info().isFictious or Vh1->info().stabilityCoefficient == 0) continue;
		const Real timeStep = -2 /( (Vh1->info().stabilityCoefficient * rho / Vh1->info().vMixture ) * Vh1->info().dpds );
		if (timeStep < 0) continue;
	 	//if (debug_precise) cout<< "dpds " << Vh1->info().dpds << " vertvolume " << Vh1->info().volume << " ts " << timeStep << endl;
		maxTimeStep = timeStep < maxTimeStep ? timeStep : maxTimeStep;
		Vh1->info().stabilityCoefficient = 0;
	}

 	const long size = Tes.cellHandles.size();
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		if (cell->info().isFictious or cell->info().stabilityCoefficient ==0) continue;
		const Real timeStep = -2 / ( (cell->info().stabilityCoefficient * rho / cell->info().vMixture) * cell->info().dpds );
		if (timeStep < 0) continue;
		//if (debug_precise) cout<< "dpds " << cell->info().dpds << " cellvolume " << cell->info().vMixture << " ts " << timeStep << endl;
		maxTimeStep = timeStep < maxTimeStep ? timeStep : maxTimeStep;
		cell->info().stabilityCoefficient = 0;
	}
	return maxTimeStep;
}

void TransportFlowEngine::buildTriangulation ( Real pZero2, Solver& flow )
{
 	if (first) flow.currentTes=0;
        else {  flow.currentTes=!flow.currentTes; if (debug) cout << "--------RETRIANGULATION-----------" << endl;}
	flow.resetNetwork();
	initSolver(flow);
        if (alphaBound<0) addBoundary ( flow ); // these bounding planes are complicating things with alpha
        triangulate ( flow );
        if ( debug ) cout << endl << "Tesselating------" << endl << endl;
        flow.tesselation().compute();
        if (alphaBound<0) flow.defineFictiousCells(); // fictious cells only exist in cuboids
	if ( debug ) cout << endl << "Tesselation computed" << endl << endl;
	// For faster loops on cells define these vectors
	flow.tesselation().cellHandles.clear();
	flow.tesselation().cellHandles.reserve(flow.tesselation().Triangulation().number_of_finite_cells());
	FiniteCellsIterator cell_end = flow.tesselation().Triangulation().finite_cells_end();
	int k=0;
	for ( FiniteCellsIterator cell = flow.tesselation().Triangulation().finite_cells_begin(); cell != cell_end; cell++ ){
		flow.tesselation().cellHandles.push_back(cell);
		cell->info().id=k++;}//define unique numbering now, corresponds to position in cellHandles
	flow.tesselation().cellHandles.shrink_to_fit();
	// for fast loop on facets and vertices ( useful in thermal/transport engine only)

	flow.tesselation().facetCells.clear();
	flow.tesselation().facetCells.reserve(flow.tesselation().Triangulation().number_of_finite_facets());
	for ( FiniteCellsIterator cell = flow.tesselation().Triangulation().finite_cells_begin(); cell != cell_end; cell++ ){
		for(int i=0;i<4;i++){
			if (cell->info().id<cell->neighbor(i)->info().id){
				flow.tesselation().facetCells.push_back(std::pair<CellHandle,int>(cell,i));
				solver->computeHydraulicRadius(cell,i); // compute new facet geometrical values while here
			}
		}
	}
	flow.tesselation().facetCells.shrink_to_fit();
	if ( debug ) cout << endl << "facets set" << endl << endl;

        flow.displayStatistics ();
	if(!blockHook.empty()){ LOG_INFO("Running blockHook: "<<blockHook); pyRunString(blockHook); }

	if (fluidBulkModulus>0) initializeVolumes(flow);  // needed for multithreaded compressible flow (old site, fixed bug https://bugs.launchpad.net/yade/+bug/1687355)
        porosity = flow.vPoralPorosity/flow.vTotalPorosity;
	//computeFacetSurfaces();
        if (alphaBound<0) boundaryConditions ( flow );
	if ( debug ) cout << endl << "bound conds done" << endl << endl;
        initializePotential ();
	solver->computePermeability(); // FIX ME: not efficient, but spent too much time debugging a faster internal version.
	if ( debug ) cout << endl << "potentials initialized" << endl << endl;
	setInitialValues();
	if ( debug ) cout << endl << "values set" << endl << endl;
	setParticleTransferBoundary();
	if ( debug ) cout << endl << "bound conds done, potentials initialized, initialvalues set, particle transfer boundary" << endl << endl;


        if ( !first && !multithread && (useSolver==0 || fluidBulkModulus>0 || doInterpolate || thermalEngine || transportEngine)){
		interpolateTesselations ( flow.T[!flow.currentTes], flow.tesselation(), flow );
		if ( debug ) cout << endl << "cells interpolated" << endl << endl;
		interpolateVertices( flow.T[!flow.currentTes], flow.tesselation() );
		if ( debug ) cout << endl << "vertices interpolated" << endl << endl;
	}

	// update the diffusion terms of the particle and cell network
	// setVerticesConductivities();
	// if (debug) cout << "set conductivities" << endl;
	// setCellConductivities();
	flow.sphericalVertexAreaCalculated = false;
}

// need a function that updates the facet surfaces based on particle positiosn, not vertex positiosn. this allows us to avoid remeshing.
void TransportFlowEngine::updateFacetSurfaces()
{

	Tesselation& Tes = solver->T[solver->currentTes];
	RTriangulation Tri = Tes.Triangulation();
	const long size = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		if ((cell->info().isFictious) || cell->info().blocked) continue;
		Point& p1 = cell->info();

		for (int j;j<3;j++){
			Point& p2 = cell->neighbor(j)->info();
			VertexHandle W[3];
			for (int kk = 0; kk < 3; kk++) {
				W[kk] = cell->vertex(facetVertices[j][kk]);
			}
			vector<Vector3r> V(4);
			//vector<CVector> V(4);
			Real r[3];
		        int b=0;
		        int w=0;
		        cell->info().volumeSign=1;
		        Real Wall_coordinate=0;

		        for ( int y=0;y<4;y++ ) {
		                if ( ! ( cell->vertex ( y )->info().isFictious ) ) {
		                        for (int z=0;z<3;z++) V[w][z]=positionBufferCurrent[cell->vertex ( y )->info().id()].pos[z];
					r[w]=positionBufferCurrent[cell->vertex ( y )->info().id()].radius;
					w++;
		                } else {
		                        b = cell->vertex ( y )->info().id();
		                        const shared_ptr<Body>& wll = Body::byId ( b , scene );
		                        if ( !solver->boundary ( b ).useMaxMin ) Wall_coordinate = wll->state->pos[solver->boundary ( b ).coordinate]+ ( solver->boundary ( b ).normal[solver->boundary ( b ).coordinate] ) *wallThickness/2.;
		                        else Wall_coordinate = solver->boundary ( b ).p[solver->boundary ( b ).coordinate];
		                }
		        }

			// facetSurfaces
			CVector V0 = makeCgVect(V[0]);
			CVector V1 = makeCgVect(V[1]);
			CVector V2 = makeCgVect(V[2]);
			CVector V3 = makeCgVect(V[3]);

			cell->info().facetSurfaces[j]
			        = 0.5 * CGAL::cross_product(V0 - V3, V1 - V3);
			if (cell->info().facetSurfaces[j][0] == 0 && cell->info().facetSurfaces[j][1] == 0 && cell->info().facetSurfaces[j][2] == 0)
				cerr << "NULL FACET SURF" << endl;
			if (cell->info().facetSurfaces[j] * (p2 - p1) > 0) cell->info().facetSurfaces[j] = -1.0 * cell->info().facetSurfaces[j];

			// facetFluidSurfacesRatio

			cell->info().facetSphereCrossSections[j] = CVector(
			        W[0]->info().isFictious ? 0
			                                : 0.5 * r[0]
			                        * acos((V1 - V0 ) * (V2 - V0 )
			                               / sqrt((V1 - V0).squared_length()
			                                      * (V2 - V0).squared_length())),
			        W[1]->info().isFictious ? 0
			                                : 0.5 * r[1]
			                        * acos((V0 - V1) * (V2 - V1)
			                               / sqrt((V1 - V0).squared_length()
			                                      * (V2 - V1).squared_length())),
			        W[2]->info().isFictious ? 0
			                                : 0.5 * r[2]
			                        * acos((V0 - V2) * (V1 - V2)
			                               / sqrt((V1 - V2).squared_length()
		                                      * (V2 -V0).squared_length())));
		}
	}
}


void TransportFlowEngine::boundaryConditions ( Solver& flow )
{
	for (int k=0;k<6;k++)	{
		flow.cellTransferBoundary (wallIds[k]).fluxCondition=!cellBndCondIsDirichlet[k];
                flow.cellTransferBoundary (wallIds[k]).value=cellTransferBndCondValue[k];
	}
}

void TransportFlowEngine::initializePotential()
{
	RTriangulation&  Tri = solver->T[solver->currentTes].Triangulation();
	FiniteCellsIterator cellEnd = Tri.finite_cells_end();

	for (FiniteCellsIterator cell = Tri.finite_cells_begin(); cell != cellEnd; cell++) {
		if (!cell->info().Pcondition) cell->info().potential = cellP0;
		cell->info().dv() = 0;
	}

	for (int bound = 0; bound < 6; bound++) {
		int& id = *solver->boundsIds[bound];
		solver->boundingCells[bound].clear();
		if (id < 0) continue;
		CGT::TransportBoundary& bi = solver->cellTransferBoundary(id);
		if (!bi.fluxCondition) {
			VectorCell tmpCells;
			tmpCells.resize(10000);
			VCellIterator cells_it = tmpCells.begin();
			VCellIterator cells_end = Tri.incident_cells(solver->T[solver->currentTes].vertexHandles[id], cells_it);
			for (VCellIterator it = tmpCells.begin(); it != cells_end; it++) {
				//cout << "setting boundary cell " << (*it)->info().id << " with bi value " << bi.value << endl;
				(*it)->info().potential = bi.value;
				(*it)->info().Pcondition = true;
				solver->boundingCells[bound].push_back(*it);
			}
		}
		solver->boundingCells[bound].shrink_to_fit();
	}


}

void TransportFlowEngine::setInitialValues()
{
	Tesselation& Tes = solver->T[solver->currentTes];

	YADE_PARALLEL_FOREACH_BODY_BEGIN(const auto& b , scene->bodies) {
		if (!b) continue;
		const Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh = Tes.vertex(b->getId());
		if (first) Vh->info().vMixtureOriginal = pow(sphere->radius,3) * 4 / 3 * Mathr::PI;
		Vh->info().vMixture = Vh->info().vMixtureOriginal;
		if (first) Vh->info().radiiOriginal = sphere->radius;
		Vh->info().potential = particleP0;
		Vh->info().Po = Po;
		Vh->info().lambdao = lmbda;
		//Vh->info().saturation = vanGenuchten(Vh, -Vh->info().potential);
		//cout << "saturation from vg " << Vh->info().saturation << endl;
		//Vh->info().massWater = (Vh->info().saturation * (Vh->info().vMixture - Vh->info().vSolids)) * rho; // WRONG?

	}
	YADE_PARALLEL_FOREACH_BODY_END();

 	const long size = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		cell->info().Po = Po;
		cell->info().lambdao = lmbda;
		cell->info().initialDryDensity = initialDryDensity;
		//cell->info().saturation = vanGenuchten(cell, -cell->info().potential);
	}
}

void TransportFlowEngine::computeVertexSphericalArea()
{
	Tesselation& Tes = solver->T[solver->currentTes];
	const long size = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		if ((ignoreFictiousTransfer && cell->info().isFictious) || cell->info().blocked) continue;

		VertexHandle W[4];
		for (int k = 0; k < 4; k++)
			W[k] = cell->vertex(k);
		if (cell->vertex(0)->info().isFictious) cell->info().sphericalVertexSurface[0] = 0;
		else
			cell->info().sphericalVertexSurface[0]
			        = solver->fastSphericalTriangleArea(W[0]->point(), W[1]->point().point(), W[2]->point().point(), W[3]->point().point());
		if (cell->vertex(1)->info().isFictious) cell->info().sphericalVertexSurface[1] = 0;
		else
			cell->info().sphericalVertexSurface[1]
			        = solver->fastSphericalTriangleArea(W[1]->point(), W[0]->point().point(), W[2]->point().point(), W[3]->point().point());
		if (cell->vertex(2)->info().isFictious) cell->info().sphericalVertexSurface[2] = 0;
		else
			cell->info().sphericalVertexSurface[2]
			        = solver->fastSphericalTriangleArea(W[2]->point(), W[1]->point().point(), W[0]->point().point(), W[3]->point().point());
		if (cell->vertex(3)->info().isFictious) cell->info().sphericalVertexSurface[3] = 0;
		else
			cell->info().sphericalVertexSurface[3]
			        = solver->fastSphericalTriangleArea(W[3]->point(), W[1]->point().point(), W[2]->point().point(), W[0]->point().point());
	}
	solver->sphericalVertexAreaCalculated = true;
}


void TransportFlowEngine::resetBoundaryFluxSums()
{
	for (int i = 0; i < 6; i++)
		particleTransferBndFlux[i] = 0;
}

void TransportFlowEngine::setParticleTransferBoundary()
{
	for (int k = 0; k < 6; k++) {
		solver->particleTransferBoundary(wallIds[k]).fluxCondition = !particleBndCondIsDirichlet[k];
		solver->particleTransferBoundary(wallIds[k]).value         = particleTransferBndCondValue[k];
	}

	RTriangulation&                  Tri    = solver->T[solver->currentTes].Triangulation();
	for (int bound = 0; bound < 6; bound++) {
		int& id = *solver->boundsIds[bound];
		solver->particleTransferBoundingCells[bound].clear();
		if (id < 0) continue;
		CGT::TransportBoundary& bi = solver->particleTransferBoundary(id);

		if (!bi.fluxCondition) {
			VectorCell tmpCells;
			tmpCells.resize(10000);
			VCellIterator cells_it  = tmpCells.begin();
			VCellIterator cells_end = Tri.incident_cells(solver->T[solver->currentTes].vertexHandles[id], cells_it);

			for (VCellIterator it = tmpCells.begin(); it != cells_end; it++) {
				CellHandle& cell = *it;
				for (int v = 0; v < 4; v++) {
					if (!cell->vertex(v)->info().isFictious) {
						cell->vertex(v)->info().potential = bi.value;
						cell->vertex(v)->info().Pcondition = true;
					}
				}
				solver->particleTransferBoundingCells[bound].push_back(cell);
			}
		}
	}
	boundarySet = true;
}


/////// Particle to cell transfer ///////


void TransportFlowEngine::computeParticleCellTransfer()
{
	if (!solver->sphericalVertexAreaCalculated) computeVertexSphericalArea();
	Tesselation&               Tes    = solver->T[solver->currentTes];

	const long size = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		//	#else
		if (cell->info().isFictious and !cell->info().Pcondition) continue;
		for (int v = 0; v < 4; v++) {
			// if (cell->vertex(v)->info().isFictious) continue;
			//if (!cell->info().Pcondition && cell->info().isFictious)
			//	continue; // don't compute particleTransfer with boundary cells that do not have a temperature assigned
			const VertexHandle& Vh = cell->vertex(v);
			const shared_ptr<Body>&  b = (*scene->bodies)[Vh->info().id()];
			//if (Tri.is_infinite(Vh)) continue;
			// if (Vh->info().isFictious) continue;
			const Real surfaceArea = cell->info().sphericalVertexSurface[v];
			computeParticleCellFlux(cell, Vh, b, surfaceArea); // keep separate so user can possibly easily find and modify this function
		}
	}
}


void TransportFlowEngine::computeParticleCellFlux(CellHandle& cell, const VertexHandle& v, const shared_ptr<Body>& b, const Real surfaceArea)
{

	Real diffusion = computeParticleCellDiffusion(v, cell, b, surfaceArea);
	if (cell->info().isFictious) diffusion = 0;
	const Real pot_diff = (v->info().potential - cell->info().potential);
	Real flux = diffusion * pot_diff;
 	if (pot_diff == 0) return;

	if (isnan(flux) or isinf(flux)) flux = 0;

	if (!cell->info().Pcondition or !cell->info().isFictious or !cell->info().blocked) {
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
		cell->info().fluxSum += flux;
		if (elapsedIters == checkStability) cell->info().stabilityCoefficient += diffusion;
	}
	if (!v->info().Pcondition) {
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
		v->info().fluxSum -= flux;
		if (elapsedIters == checkStability) v->info().stabilityCoefficient += diffusion;
	}
}



/////// Particle particle transfer ///////

void TransportFlowEngine::computeParticleParticleTransfer()
{
	Tesselation&  Tes    = solver->T[solver->currentTes];
	const shared_ptr<InteractionContainer>& interactions = scene->interactions;
	const long                              size         = interactions->size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		const shared_ptr<Interaction>& I = (*interactions)[i];
		const ScGeom* geom;
		if (!I || !I->geom.get() || !I->phys.get() || !I->isReal()) continue;
		if (I->geom.get()) {
			geom = YADE_CAST<ScGeom*>(I->geom.get());
			if (!geom) continue;
			const VertexHandle& Vh1 = Tes.vertex(I->getId1());
			const VertexHandle& Vh2 = Tes.vertex(I->getId2());
			const shared_ptr<Body>&  b1 = (*scene->bodies)[I->getId1()];
			const shared_ptr<Body>&  b2 = (*scene->bodies)[I->getId2()];
			const Sphere* sphere1 = dynamic_cast<Sphere*>(b1->shape.get());
			const Sphere* sphere2 = dynamic_cast<Sphere*>(b1->shape.get());
			if (!sphere1 or !sphere2) continue;
			const Real diffusion = computeParticleParticleDiffusion(Vh1, Vh2, b1, b2); // separate function so user can modify
			const Real pot_diff = (Vh1->info().potential - Vh2->info().potential) ;
			if (pot_diff == 0) continue;
			Real flux = diffusion *  pot_diff ;
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
			Vh1->info().fluxSum -= flux;
			if (elapsedIters == checkStability) Vh1->info().stabilityCoefficient += diffusion;
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
			Vh2->info().fluxSum += flux;
			if (elapsedIters == checkStability) Vh2->info().stabilityCoefficient += diffusion;

		}
	}
}

Real TransportFlowEngine::computeParticleParticleDiffusion(const VertexHandle& Vh1, const VertexHandle& Vh2, const shared_ptr<Body>& b1, const shared_ptr<Body>&  b2)
{
	const Real conductivity1 = Vh1->info().conductivity;
	const Real conductivity2 = Vh2->info().conductivity;
	const Real avg_radius = ( Vh1->point().weight() + Vh2->point().weight() )/ 2 ;
	const Real area = pow(avg_radius,2) * Mathr::PI ;
	const Vector3r l = b1->state->pos - b2->state->pos;

	Real distance = sqrt(l.squaredNorm());
	const Real length = distance < minimumLength ? minimumLength : distance;
	const Real diffusion = particleParticleDiffusionCoefficient * ( (conductivity1+conductivity2) / 2 ) * ( area / length );
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
	avgParticleDiffusion += diffusion;
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
	numParticleDiffusion += 1;
	if (isnan(diffusion) or isinf(diffusion)) return 0;
	else return diffusion;
}

Real TransportFlowEngine::computeCellCellDiffusion(const CellHandle& cell1, const CellHandle& cell2, const int j)
{
	const Real conductivity1 = cell1->info().conductivity;
	const Real conductivity2 = cell2->info().conductivity;

	const CVector& Surfk = cell1->info().facetSurfaces[j];
	Real           area = sqrt(Surfk.squared_length());
	const CVector l = cell1->info() - cell2->info();

	Real distance = sqrt(l.squared_length());
	const Real length = distance < minimumLength ? minimumLength : distance;
	const Real diffusion = cellCellDiffusionCoefficient * ( (conductivity1+conductivity2) / 2 ) * ( area / length ); //* (distance/area); SWITCHBACK
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
	avgCellDiffusion += diffusion;
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
	numCellDiffusion += 1;

	if (isnan(diffusion) or isinf(diffusion)) return 0;
	else return diffusion;
}

Real TransportFlowEngine::computeParticleCellDiffusion(const VertexHandle& Vh1, const CellHandle& cell2, const shared_ptr<Body>& b1, const Real surfaceArea)
{
	const Real conductivity1 = Vh1->info().conductivity;
	const Real conductivity2 = cell2->info().conductivity;
	const Vector3r b1_pos = b1->state->pos;
	const Vector3r cell2_pos(cell2->info()[0], cell2->info()[1], cell2->info()[2]);
	const Vector3r l = b1_pos - cell2_pos;

	Real distance = sqrt(l.squaredNorm());
	const Real length = distance < minimumLength ? minimumLength : distance;
	const Real diffusion = particleCellDiffusionCoefficient * ( (conductivity1+conductivity2) / 2 )  * ( surfaceArea / length );
	if (isnan(diffusion) or isinf(diffusion)) return 0;
	else return diffusion;
}

//// Updating Vertex values /////

void TransportFlowEngine::setVerticesConductivities()
{
	Tesselation& Tes = solver->T[solver->currentTes];

	YADE_PARALLEL_FOREACH_BODY_BEGIN(const auto& b, scene->bodies) {
		if (!b) continue;
		const Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh1 = Tes.vertex(b->getId());
		Vh1->info().conductivity = getVertexConductivity(Vh1);
	}
	YADE_PARALLEL_FOREACH_BODY_END();
}

void TransportFlowEngine::readConductivityFile() {

	std::ifstream file;
	file.open(conductivityTextFile);
	oneOverRhoTimesGravity = 1 / (rho * gravity);
	if (file) {
		if (suctions.empty()) {
			Real	x, y;
			while (file >> x >> y) {
				suctions.push_back(x);
				conductivities.push_back(y * oneOverRhoTimesGravity * conductivityFactor); // converting to permeability/viscosity here to avoid duplicating divisions
			}
			useCondTextFile = true;
			cout << "using user defined conductivity file for pellet conductivity" << endl;
		} else {
			cout << "suction vector filled already for conductivity file" << endl; }
	} else {
		cerr << "conductivity text file path or not provided or unopenable" << endl;
		useCondTextFile = false;
	}


}

Real TransportFlowEngine::getVertexConductivity(const VertexHandle& Vh)
{
	if (useCondTextFile) {
		const Real potential = -Vh->info().potential;
		const Real conductivity = interpolate(suctions, conductivities, potential, false);
		return conductivity;
	} else {
		return particleConductivity;
	}


}

Real TransportFlowEngine::interpolate( vector<Real> &xData, vector<Real> &yData, Real x, bool extrapolate )
{
   int size = xData.size();

   int i = 0;                                                                  // find left end of interval for interpolation
   if ( x >= xData[size - 2] )                                                 // special case: beyond right end
   {
      i = size - 2;
   }
   else
   {
      while ( x > xData[i+1] ) i++;
   }
   Real xL = xData[i], yL = yData[i], xR = xData[i+1], yR = yData[i+1];      // points on either side (unless beyond ends)
   if ( !extrapolate )                                                         // if beyond ends of array and not extrapolating
   {
      if ( x < xL ) yR = yL;
      if ( x > xR ) yL = yR;
   }

   Real dydx = ( yR - yL ) / ( xR - xL );                                    // gradient
   //cout << "interpolated value" << yL + dydx * ( x - xL ) << endl;
   return yL + dydx * ( x - xL );                                              // linear interpolation
}

///// Updating Cell values /////

void TransportFlowEngine::setCellConductivities()
{
	Tesselation& Tes = solver->T[solver->currentTes];
	const long size = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i2 = 0; i2 < size; i2++) {
		CellHandle& cell = Tes.cellHandles[i2];
		cell->info().conductivity = getCellConductivity(cell);
	}
}

Real TransportFlowEngine::getCellConductivity(const CellHandle& cell)
{
	const Real krM = (cell->info().saturation - SrM) / (SsM - SrM);
	const Real kM = cellConductivity * exp(b*(cell->info().porosity - cell->info().initialPorosity));
	return krM*kM;
}

void TransportFlowEngine::setCellsDPDS(Solver& flow)
{
	Tesselation& Tes = flow.T[flow.currentTes];
	maxDSDPj = 0;
	const long size = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		Real        deriv;
		deriv = dpds(cell);// dsdp(cell);
		if ( deriv > maxDSDPj ) maxDSDPj = deriv;
		cell->info().dpds = deriv;
	}
}

void TransportFlowEngine::setVerticesDPDS(Solver& flow)
{
	maxDSDPj = 0;
	Tesselation& Tes = flow.T[flow.currentTes];

YADE_PARALLEL_FOREACH_BODY_BEGIN(const auto& b, scene->bodies) {
		if (!b) continue;
		const VertexHandle& Vh = Tes.vertex(b->getId());
		const Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		Real        deriv;
		deriv = dpds(Vh); //dsdp(Vh);
		if ( deriv > maxDSDPj ) maxDSDPj = deriv;
		if (freezeSaturation) deriv = 0;
		Vh->info().dpds = deriv;
	}
YADE_PARALLEL_FOREACH_BODY_END();
}


void TransportFlowEngine::setSaturationFromPcS(CellHandle& cell)
{
	Real pc = pAir - cell->info().potential; // suction in macrostructure
	Real saturation;
	if (pc >= 0)
		saturation = vanGenuchten(cell, pc);
	else
		saturation = 1.;
	if (saturation < SrM)
		saturation = SrM;
	if (saturation > SsM)
		saturation = SsM;
	cell->info().saturation        = saturation;
	cell->info().initialSaturation = saturation;
}


///// Cell cell transfer /////



void TransportFlowEngine::computeCellCellFluxes()
{
	Tesselation& Tes        = solver->T[solver->currentTes];
	const long   sizeFacets = Tes.facetCells.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < sizeFacets; i++) {
		std::pair<CellHandle, int> facetPair    = Tes.facetCells[i];
		const CellHandle&          cell         = facetPair.first;
		const CellHandle&          neighborCell = cell->neighbor(facetPair.second);
		if ((neighborCell->info().isFictious and !neighborCell->info().Pcondition) or (cell->info().isFictious and !cell->info().Pcondition)) continue;
		const Real delta_potential = cell->info().potential - neighborCell->info().potential;
		const Real diffusion = computeCellCellDiffusion(cell, neighborCell, facetPair.second);
		Real flux = diffusion * (delta_potential) ;
		if (isnan(flux)){
			cout << "flux " << flux << " diffusion" <<  diffusion << " cell pot  " << cell->info().potential << " neighbor pot " << neighborCell->info().potential << endl;
			getchar();
			}
		// if (delta_potential == 0) return;
		//if ((cell->info().Pcondition and !neighborCell->info().Pcondition) or (!cell->info().Pcondition and neighborCell->info().Pcondition)) cout << "cellcell flux " << flux << " cell1 potential " << cell->info().potential << " cell2 potential " << neighborCell->info().potential << endl;
		//cout << "flux " << flux << " diffusion " << diffusion << " cell " << cell->info().potential << " ncell " << neighborCell->info().potential << endl;

		if (!cell->info().Pcondition and !cell->info().isFictious) {
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
			cell->info().fluxSum -= flux ;
			if (elapsedIters == checkStability) cell->info().stabilityCoefficient += diffusion ;
		}

		if (!neighborCell->info().Pcondition and !neighborCell->info().isFictious) {
#ifdef YADE_OPENMP
#pragma omp atomic
#endif
			neighborCell->info().fluxSum += flux ;
			if (elapsedIters == checkStability) neighborCell->info().stabilityCoefficient += diffusion;
		}

	}

}

CVector TransportFlowEngine::cellBarycenter(const CellHandle& cell)
{
	CVector center(0, 0, 0);
	for (int k = 0; k < 4; k++)
		center = center + 0.25 * (cell->vertex(k)->point().point() - CGAL::ORIGIN);
	return center;
}

//// Forward difference the particles /////

void TransportFlowEngine::setNewParticlePotentials()
{
	Tesselation& Tes = solver->T[solver->currentTes];
	//FOREACH ( const shared_ptr<Body>& b, *scene->bodies ) {
	YADE_PARALLEL_FOREACH_BODY_BEGIN (const auto& b, scene->bodies) {
		if (!b) continue;
		Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh = Tes.vertex(b->getId());
		if (Vh->info().Pcondition) continue;
		//const Real volume = Vh->info().volume;
		Vh->info().oldMassWater = Vh->info().massWater;
 		Vh->info().massWater = Vh->info().fluxSum * rho * scene->dt + Vh->info().oldMassWater;
		if (iterativeSubStepFactor>0) minimizeInconsistencies(Vh, sphere); // this also solves vol strain and porosity automatically
		else if (useNewtonRaphson) {
			if (debug_precise) cout << "particle raphson" << endl;
			solveWithNewtonRaphson(Vh);
			Vh->info().vStrain = Vh->info().vMixture / Vh->info().vMixtureOriginal - 1;
			const Real rNew = pow(3. * Vh->info().vMixture / (4. * M_PI), 1. / 3.);
			Vh->info().radiiChange = rNew - Vh->info().radiiOriginal;
			sphere->radius = rNew;
		} else {
			Vh->info().saturation = ( Vh->info().massWater / rho ) / (Vh->info().vMixture); // - Vh->info().vSolids);
			//cout << "vertex pot before van gen " << Vh->info().potential << " lambdao " << Vh->info().lambdao << " Po " << Vh->info().Po << endl;
			Vh->info().potential = -vanGenuchten_pressure(Vh, Vh->info().saturation);
			//cout << "oldmassWater " << Vh->info().oldMassWater << " volume " << Vh->info().volume << "saturation " << Vh->info().saturation << " potential " << Vh->info().potential << " massWater " << Vh->info().massWater << " fluxsum " << Vh->info().fluxSum << endl ;
		}
		Vh->info().fluxSum = 0; // reset for next time step collection
	}
	YADE_PARALLEL_FOREACH_BODY_END();
}

////// Forward difference the cells //////

void TransportFlowEngine::setNewCellPotentials()
{
	Tesselation& Tes = solver->T[solver->currentTes];
	const long size = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i2 = 0; i2 < size; i2++) {
		CellHandle& cell = Tes.cellHandles[i2];
		if (cell->info().isFictious or cell->info().blocked or cell->info().Pcondition) continue;

		cell->info().oldMassWater = cell->info().massWater;
 		cell->info().massWater = cell->info().fluxSum * rho * scene->dt + cell->info().oldMassWater;
		if (iterativeSubStepFactor>0) minimizeInconsistencies(cell); // this also solves the volumetric strain and porosity automatically
		else if (useNewtonRaphson){
			if (debug_precise) cout << "cell raphson" << endl;
 			solveWithNewtonRaphson(cell);
			cell->info().vMixtureChange = cell->info().vMixture - cell->info().vMixtureOriginal;
			cell->info().vStrain = cell->info().vMixture / cell->info().vMixtureOriginal - 1;
		} else {
			const Real saturation = ( cell->info().massWater / rho ) / (cell->info().vMixture); // - cell->info().vSolids); // vMixture
			if (saturation > maxClampSaturation) cell->info().saturation = maxClampSaturation;
			else if (saturation < minClampSaturation) cell->info().saturation= minClampSaturation;
			else cell->info().saturation = saturation;
			cell->info().potential = -vanGenuchten_pressure(cell, cell->info().saturation);
			cell->info().vWater = cell->info().massWater / rho;
		}
		//if (debug_precise) cout << "oldmassWater " << cell->info().oldMassWater << " vmixture " << cell->info().vMixture << " vmixtureorig " << cell->info().vMixtureOriginal << "saturation " << cell->info().saturation << " potential " << cell->info().potential << " massWater " << cell->info().massWater << " fluxsum " << cell->info().fluxSum << endl ;
		cell->info().fluxSum = 0; // reset for next time step collection



	}
}

void TransportFlowEngine::minimizeInconsistencies(CellHandle& cell)
{
	Real saturation, saturation_new, potential, potential_new, volume_new, inconsistency_1, inconsistency_2, inconsistency_3;
	//inconsistency = 1e10; // big number
	Real volumeMixture = cell->info().vMixture;
	const Real voidVolume = 1 / cell->info().invVoidVolume();
	Real swelledVol;
	int n = 0;
	while (n<1000)
	{

		saturation = ( cell->info().massWater / rho ) / (volumeMixture); // - cell->info().vSolids);
		cell->info().porosity = 1. - cell->info().vSolids / volumeMixture ;
		cell->info().Po = Po * exp(a * (meanInitialPorosity - cell->info().porosity));
		cell->info().lambdao = lmbda * exp(b * (meanInitialPorosity - cell->info().porosity));
		potential = -vanGenuchten_pressure(cell, saturation);
		swelledVol = volume_from_pressure(cell, potential);

		saturation_new = ( cell->info().massWater / rho ) / (swelledVol); // - cell->info().vSolids);
		cell->info().porosity = 1. - cell->info().vSolids / swelledVol ;
		cell->info().Po = Po * exp(a * (meanInitialPorosity - cell->info().porosity));
		cell->info().lambdao = lmbda * exp(b * (meanInitialPorosity - cell->info().porosity));
		potential_new = -vanGenuchten_pressure(cell, saturation_new);
		volume_new = volume_from_pressure(cell, potential_new);

		inconsistency_1 = (potential - potential_new) / potential;
		inconsistency_2 = (saturation - saturation_new) / saturation;
		inconsistency_3 = (swelledVol - volume_new) / swelledVol;

		volumeMixture = volume_new;
		if(debug_precise) cout << "incells, inc1 " << inconsistency_1 << " inc2 " << inconsistency_2 << " inc3 " << inconsistency_3 << endl;
		n += 1;
		if (std::abs(inconsistency_1) < iterativeSubStepFactor) break;
		if (std::abs(inconsistency_2) < iterativeSubStepFactor) break;
		if (std::abs(inconsistency_3) < iterativeSubStepFactor) break;
	}
	//cell->info().vMixture = swelledVol;
	cell->info().saturation = saturation_new;
	cell->info().potential = potential_new;

	cell->info().vMixture = swelledVol < voidVolume ? swelledVol : voidVolume; // dont let it swell beyond the available volume FIXME: add criteria in loop to incorporate this
	cell->info().vMixtureChange = cell->info().vMixture - cell->info().vMixtureOriginal;
	cell->info().vStrain = cell->info().vMixture / cell->info().vMixtureOriginal - 1;

}

void TransportFlowEngine::solveWithNewtonRaphson(auto system)
{
	// initial guess is based on previous vMixture and current mass water.
	Real s = system->info().massWater / ((system->info().vMixture - system->info().vSolids) * rho);
	if (debug_precise) cout << "system vsolids " << system->info().vSolids << " vMixture " << system->info().vMixture << " mass water " << system->info().massWater << " fluxsum " << system->info().fluxSum << endl;
	if (debug_precise) cout << "lambda " << system->info().lambdao << " p " << system->info().Po << endl;
	Real p = vanGenuchten_pressure_general(system, s); // try with suction
	const Real suction0 = pAir - pZero;
	Real V = system->info().vMixtureOriginal * ((betam/alpham) * (exp(-alpham * p) - exp(-alpham * suction0)) + 1);
	if (debug_precise) cout << "in raphson s " << s <<" p " << p << " V " << V << " vs vmix "<<system->info().vMixture << endl;

	EMatrix2r J(2, 2);
	EVector2r R;
	EVector2r dx;
	EVector2r xold(p,V); //xold(s,V,p);
	EVector2r x(p,V); //x(s,V,p);
	//const Real h = 0.001;
	Real Vfront,Vback,pfront,pback; //sfront, sback,
	//const Real errorThreshold = 0.05;
	Real sum;
	Real RMS = 1;
	Real relaxFactor = 1;
	int n = 0;
	Real mu = mu_start;
	while (RMS > errorThreshold or n > 100)
	{
		relaxFactor = relaxFactor * exp(-lambda_relax * n); // exponentially decay the relaxation factor
		// s = x[0];
 		V = x[1]; p = x[0];
		//Real hs = s*hfactor;
		Real hV = V*hfactor;
		Real hp = p*hfactor;
		// compute residual
		//R(2) = functionVanGenuchten(system, x[0], x[1], x[2]);
		//R(0) = functionSaturation(system, x[0], x[1], x[2]);
		//R(1) = functionVolume(system, x[0], x[1], x[2]);

		// compute the constraints
		// Real c1 = 0; Real c2 = 0; Real c3 = 0; Real c4 = 0; Real c5 = 0 ; Real sumC=0;
		// if (constrain_newton){
		// 	mu = mu * mu;
		// 	c1 = log(1-system->info().vSolids / V ); // enforcing porosity >= 0
		// 	c2 = log(system->info().vSolids / V); // enforcing porosity <= 1
		// 	if (system->info().isCell) c3 = log(1/system->info().invVoidVolume() - V); // enforcing volume mixture <= volume cell
		// 	c4 = log(maxSuctionConstraint - p);
		// 	c5 = log(p - minSuctionConstraint);
		// 	sumC = c1 + c2 + c3 + c4 + c5;
		// 	sumC = mu * sumC;
		// }
		// if (debug_precise) cout << " sumC " << sumC << " mu " << mu << endl;

		// PRESSURE VOLUME
		R(0) = function1(system, x[1], x[0],mu);
		R(1) = function2(system, x[1], x[0],mu);
		if (debug_precise) cout << "setting residual R " << R << endl;
		// prepare values for numerical derivative
		pback = x[0]-hp; // functionVanGenuchten(system, s-h, V-h, p-h);
		//sback = x[0]-hs; //functionSaturation(system,s-h, V-h, p-h);
		Vback = x[1]-hV; //functionVolume(system, s-h, V-h, p-h)
		pfront = x[0]+hp; //functionVanGenuchten(system, s-h, V-h, p-h);
		//sfront = x[0]+hs;// functionSaturation(system,s-h, V-h, p-h);
		Vfront = x[1]+hV; //functionVolume(system, s-h, V-h, p-h)

		// mu1 = getConstraintSum(V,p)
		J(0,0) = (function1(system,V,pfront,mu) - function1(system,V,pback,mu)) / (2 * hp); // set this constant to 1?
		J(0,1) = (function1(system,Vfront,p,mu) - function1(system,Vback,p,mu)) / (2 * hV);

		J(1,0) = (function2(system,V,pfront,mu) - function2(system,V,pback,mu)) / (2 * hp);
		J(1,1) = (function2(system,Vfront,p,mu) - function2(system,Vback,p,mu)) / (2 * hV); // set this constant to 1?

		// J(0,0) = 1;
		// J(0,1) = dfunction1dv(system,V);
		//
		// J(1,0) = dfunction2dp(system,p);
		// J(1,1) = 1;

		// J(2,0) = (functionVanGenuchten(system,sfront,V,p) - functionVanGenuchten(system,sback,V,p)) / (2 * hs);
		// J(2,1) = (functionVanGenuchten(system,s,Vfront,p) - functionVanGenuchten(system,s,Vback,p)) / (2 * hV);
		// J(2,2) = (functionVanGenuchten(system,s,V,pfront) - functionVanGenuchten(system,s,V,pback)) / (2 * hp);
		//
		// J(0,0) = (functionSaturation(system,sfront,V,p) - functionSaturation(system,sback,V,p)) / (2 * hs);
		// J(0,1) = (functionSaturation(system,s,Vfront,p) - functionSaturation(system,s,Vback,p)) / (2 * hV);
		// J(0,2) = 0; //functionSaturation(system,s,V,pfront) - functionSaturation(system,sback,V,pback) / 2 * h;
		//
		// J(1,0) = 0; //functionVolume(system,sfront,V,p) - functionVolume(system,sback,V,p) / 2 * h;
		// J(1,1) = (functionVolume(system,s,Vfront,p) - functionVolume(system,s,Vback,p)) / (2 * hV);
		// J(1,2) = (functionVolume(system,s,V,pfront) - functionVolume(system,s,V,pback)) / (2 * hp);

		if (debug_precise) cout << "setting jacobian J " << endl << J << endl;

		if (newtonUseLdlt) dx = J.householderQr().solve(-R);
		else dx = J.colPivHouseholderQr().solve(-R); // J.ldlt().solve(-R); TRYME ldlt

		if (debug_precise) cout << "dx " << dx << endl;
		xold = x;
		x = x + relaxFactor*dx;
		if (debug_precise) cout << "x " << x << " xold " << xold << " relaxfactor " << relaxFactor << endl;
		sum = 0;
		//if (debug_precise) cout << " checking c++ math " <<  pow((x[0]-xold[0]) / xold[0],2) << endl;
		for (int i=0;i<2;i++){ sum = sum + pow((x[i]-xold[i]) / xold[i],2); }
		//cout<< "sum is " << sum << endl;
		RMS = pow(0.3333 * sum, 0.5);
		mu = 0.1*mu;
		//cout << " RMS is " << RMS << endl;
		n = n + 1;
	}
	if (n>100) cout << "newton failed to converge" << endl;
	if (debug_precise) cout << "found s,v,p with error " << RMS << " n " << n << endl;
	system->info().vMixture = x[1]; system->info().potential = -x[0]; // newton solves for suction. mass transfer wants raw pressure
	if (isnan(system->info().potential)) {
		cout << "potential assigned" <<  system->info().potential << " vmix assigned " << system->info().vMixture << endl;
		getchar();
		}
	system->info().saturation = system->info().massWater / ((V - system->info().vSolids) * rho) ;
	//system->info().saturation = x[0]; system->info().vMixture = x[1]; system->info().potential = x[2];
	system->info().porosity = 1 - system->info().vSolids/system->info().vMixture;
	system->info().Po = Po * exp(this->a*(meanInitialPorosity + system->info().vSolids/system->info().vMixture - 1));
	system->info().lambdao = lmbda * exp(this->b*(meanInitialPorosity + system->info().vSolids/system->info().vMixture - 1));
}


// Real TransportFlowEngine::functionVanGenuchten(const auto system, const Real s,const Real V,const Real p)
// {
// 	const Real zeta = Po * exp(this->a*(meanInitialPorosity + system->info().vSolids/V - 1));
// 	const Real lambda = lmbda * exp(this->b*(meanInitialPorosity + system->info().vSolids/V - 1));
// 	return p - (-zeta * pow(s, -1/lambda) * pow(pow(s,-1/lambda)-1, -lambda) * ( pow(s, 1/lambda) - 1 ));
// }
//
// Real TransportFlowEngine::functionSaturation(const auto system, const Real s,const Real V,const Real p)
// {
// 	return s - system->info().massWater / ((V - system->info().vSolids) * rho) ;
// }
//
// Real TransportFlowEngine::functionVolume(const auto system, const Real s,const Real V,const Real p)
// {
// 	const Real suction = -p;
// 	const Real suction0 = pAir - pZero;
// 	return V - system->info().vMixtureOriginal * ((betam/alpham) * (exp(-alpham * suction) - exp(-alpham * suction0)) + 1) ;
// }

Real TransportFlowEngine::function1(const auto system, const Real V,const Real p,const Real mu)
{
		Real c1 = 0; Real c2 = 0; Real c3 = 0; Real c4 = 0; Real c5 = 0 ; Real sumC=0;
		if (constrain_newton){
			//mu = mu * mu;
			c1 = log(1-system->info().vSolids / V ); // enforcing porosity >= 0
			c2 = log(system->info().vSolids / V); // enforcing porosity <= 1
			if (system->info().isCell) c3 = log(1/system->info().invVoidVolume() - V); // enforcing volume mixture <= volume cell
			c4 = log(maxSuctionConstraint - p);
			c5 = log(p - minSuctionConstraint);
			sumC = c1 + c2 + c3 + c4 + c5;
			sumC = mu * sumC;
		}
	const Real zeta = Po * exp(this->a*(meanInitialPorosity + system->info().vSolids/V - 1));
	const Real lambda = lmbda * exp(this->b*(meanInitialPorosity + system->info().vSolids/V - 1));
	const Real s = system->info().massWater / ((V - system->info().vSolids) * rho) ;
	//if (debug_precise) cout << "s " << s << " zeta " << zeta << " lambda " << lambda << " V " << V << " mwater " << system->info().massWater <<  endl;
	return p - (-zeta * pow(s, -1/lambda) * pow(pow(s,-1/lambda)-1, -lambda) * ( pow(s, 1/lambda) - 1 )) - sumC;
	//return p - (zeta * pow(pow(s,-1/lambda)-1, 1-lambda));
}

Real TransportFlowEngine::dfunction1dv(const auto system, const Real V)
{
	const Real zeta = Po * exp(this->a*(meanInitialPorosity + system->info().vSolids/V - 1));
	const Real lambda = lmbda * exp(this->b*(meanInitialPorosity + system->info().vSolids/V - 1));
	const Real s = system->info().massWater / ((V - system->info().vSolids) * rho) ;
	return ((lambda-1) * zeta * pow(s,-1/(lambda-1)) * pow(pow(s,-1/lambda)-1,-lambda)) / lambda; // test negative incase we need it for suction domain
}


Real TransportFlowEngine::function2(const auto system, const Real V,const Real p, const Real mu)
{
		Real c1 = 0; Real c2 = 0; Real c3 = 0; Real c4 = 0; Real c5 = 0 ; Real sumC=0;
		if (constrain_newton){
			//mu = mu * mu;
			c1 = log(1-system->info().vSolids / V ); // enforcing porosity >= 0
			c2 = log(system->info().vSolids / V); // enforcing porosity <= 1
			if (system->info().isCell) c3 = log(1/system->info().invVoidVolume() - V); // enforcing volume mixture <= volume cell
			c4 = log(maxSuctionConstraint - p);
			c5 = log(p - minSuctionConstraint);
			sumC = c1 + c2 + c3 + c4 + c5;
			sumC = mu * sumC;
		}
	const Real suction = p;
	const Real suction0 = pAir - pZero;
	return V - (system->info().vMixtureOriginal * ((betam/alpham) * (exp(-alpham * suction) - exp(-alpham * suction0)) + 1)) - sumC ;
}

Real TransportFlowEngine::dfunction2dp(const auto system, const Real p)
{
	const Real suction = p;
	//const Real suction0 = pAir - pZero;
	return -betam * system->info().vMixtureOriginal * exp(-alpham * suction);
}


void TransportFlowEngine::minimizeInconsistencies(const VertexHandle& Vh, Sphere* sphere)
{
	Real saturation, saturation_new, potential, potential_new, volume_new, inconsistency_1, inconsistency_2, inconsistency_3;
	//inconsistency = 1e10; // big number
	Real volumeMixture = Vh->info().vMixture;
	//const Real voidVolume = 1 / cell->info().invVoidVolume();
	Real swelledVol;
	int n = 0;
	while (n<1000)
	{

		saturation = ( Vh->info().massWater / rho ) / (volumeMixture); // - Vh->info().vSolids);
		Vh->info().porosity = 1. - Vh->info().vSolids / volumeMixture ;
		Vh->info().Po = Po * exp(a * (meanInitialPorosity - Vh->info().porosity));
		Vh->info().lambdao = lmbda * exp(b * (meanInitialPorosity - Vh->info().porosity));
		potential = -vanGenuchten_pressure(Vh, saturation);
		swelledVol = volume_from_pressure(Vh, potential);


		saturation_new = ( Vh->info().massWater / rho ) / (swelledVol); // - Vh->info().vSolids);
		Vh->info().porosity = 1. - Vh->info().vSolids / swelledVol ;
		Vh->info().Po = Po * exp(a * (meanInitialPorosity - Vh->info().porosity));
		Vh->info().lambdao = lmbda * exp(b * (meanInitialPorosity - Vh->info().porosity));
		potential_new = -vanGenuchten_pressure(Vh, saturation_new);
		volume_new = volume_from_pressure(Vh, potential_new);

		inconsistency_1 = (potential - potential_new) / potential;
		inconsistency_2 = (saturation - saturation_new) / saturation;
		inconsistency_3 = (swelledVol - volume_new) / swelledVol;

		volumeMixture = volume_new;

		if(debug_precise) cout << "In pellets, inc1 " << inconsistency_1 << " inc2 " << inconsistency_2 << " inc3 " << inconsistency_3 << endl;
		n+=1;
		if (std::abs(inconsistency_1) < iterativeSubStepFactor) break;
		if (std::abs(inconsistency_2) < iterativeSubStepFactor) break;
		if (std::abs(inconsistency_3) < iterativeSubStepFactor) break;
	}
	if (n>=999) cout<< "didnt converge" <<endl;
	//Vh->info().vMixture = swelledVol;
	Vh->info().saturation = saturation_new;
	Vh->info().potential = potential_new;

	Vh->info().vMixture = volumeMixture;
	Vh->info().vStrain = Vh->info().vMixture / Vh->info().vMixtureOriginal - 1;

	const Real rNew = pow(3. * volumeMixture / (4. * M_PI), 1. / 3.);
	Vh->info().radiiChange = rNew - Vh->info().radiiOriginal;
	sphere->radius = rNew;

}

///// Partial Sat specific tools /////


Real TransportFlowEngine::volume_from_pressure(CellHandle& cell, const Real pressure)
{
	const Real suction = pAir - pressure;
	const Real suction0 = pAir - pZero;
	const Real volStrain = betam / alpham * (exp(-alpham * suction) - exp(-alpham * suction0));
	return cell->info().vMixtureOriginal * (volStrain + 1.);
}

Real TransportFlowEngine::volume_from_pressure(const VertexHandle& Vh, const Real pressure)
{
	const Real suction = pAir - pressure;
	const Real suction0 = pAir - pZero;
	const Real volStrain = betam / alpham * (exp(-alpham * suction) - exp(-alpham * suction0));
	const Real vNew = Vh->info().vMixtureOriginal * (volStrain + 1.);
	//const Real rNew = pow(3. * vNew / (4. * M_PI), 1. / 3.);
	return vNew;

	// Vh->info().radiiChange = rNew - Vh->info().radiiOriginal;
	// sphere->radius     = rNew;
	// Vh->info().volume = pow(sphere->radius,3) * 4 / 3 * Mathr::PI; // vNew;
	// Vh->info().vStrain = volStrain;
}

Real TransportFlowEngine::dpds(CellHandle& cell)
{
	const Real s = cell->info().saturation;
	const Real P = cell->info().Po;
	const Real lmbdao = cell->info().lambdao;
	return ( P * (lmbdao-1) * pow(s,-1/(lmbdao-1)) * pow(pow(s,-1/lmbdao) - 1 ,-lmbdao) )  / lmbdao;

}

Real TransportFlowEngine::dpds(const VertexHandle& Vh)
{
	const Real s = Vh->info().saturation;
	const Real P = Vh->info().Po;
	const Real lmbdao = Vh->info().lambdao;
	return ( P * (lmbdao-1) * pow(s,-1/(lmbdao-1)) * pow(pow(s,-1/lmbdao) - 1 ,-lmbdao) )  / lmbdao;

}


Real TransportFlowEngine::dsdp(CellHandle& cell)
{

	// analytical derivative of van genuchten
	const Real pc = pAir - cell->info().potential; // suction
	if (pc <= 0) return 0;
	const Real term1 = pow(pow(pc / cell->info().Po, 1. / (1. - cell->info().lambdao)) + 1., (-cell->info().lambdao - 1.));
	const Real term2 = cell->info().lambdao * pow(pc / cell->info().Po, 1. / (1. - cell->info().lambdao) - 1.);
	const Real term3 = cell->info().Po * (1. - cell->info().lambdao);
	return term1 * term2 / term3; // techncially this derivative should be negative, but we use a van genuchten fit for suction, not water pressure. Within the numerical formulation, we want the change of saturation with respect to water pressure (not suction). Which essentially reverses the sign of the derivative.

}

Real TransportFlowEngine::dsdp(const VertexHandle& Vh)
{

	// analytical derivative of van genuchten
	const Real pc = pAir - Vh->info().potential; // suction
	if (pc <= 0) return 0;
	const Real term1 = pow(pow(pc / Vh->info().Po, 1. / (1. - Vh->info().lambdao)) + 1., (-Vh->info().lambdao - 1.));
	const Real term2 = Vh->info().lambdao * pow(pc / Vh->info().Po, 1. / (1. - Vh->info().lambdao) - 1.);
	const Real term3 = Vh->info().Po * (1. - Vh->info().lambdao);
	// if (isnan(term1 * term2 / term3)) {
	// 	cout << "Po " << Vh->info().Po << " lambdao " << Vh->info().lambdao << " pc " << pc << endl;
	// 	std::cin.get();
	// }
	return term1 * term2 / term3; // techncially this derivative should be negative, but we use a van genuchten fit for suction, not water pressure. Within the numerical formulation, we want the change of saturation with respect to water pressure (not suction). Which essentially reverses the sign of the derivative.

}

Real TransportFlowEngine::vanGenuchten(CellHandle& cell, Real pc)
{
	return pow((1. + pow(pc / cell->info().Po, 1. / (1. - cell->info().lambdao))), -cell->info().lambdao);
}

Real TransportFlowEngine::vanGenuchten(const VertexHandle& Vh, Real pc)
{
	return pow((1. + pow(pc / Vh->info().Po, 1. / (1. - Vh->info().lambdao))), -Vh->info().lambdao);
}

Real TransportFlowEngine::vanGenuchten_pressure(const VertexHandle& Vh, Real s)
{
	const Real lambda = Vh->info().lambdao;
	const Real Po = Vh->info().Po;
	return -Po * pow(s, -1/lambda) * pow(pow(s,-1/lambda)-1, -lambda) * ( pow(s, 1/lambda) - 1 );
}

Real TransportFlowEngine::vanGenuchten_pressure_general(auto system, Real s)
{
	const Real lambda = system->info().lambdao;
	const Real Po = system->info().Po;
	return -Po * pow(s, -1/lambda) * pow(pow(s,-1/lambda)-1, -lambda) * ( pow(s, 1/lambda) - 1 );
}

Real TransportFlowEngine::vanGenuchten_pressure(CellHandle& cell, Real s)
{
	const Real lambda = cell->info().lambdao;
	const Real Po = cell->info().Po;
	return -Po * pow(s, -1/lambda) * pow(pow(s,-1/lambda)-1, -lambda) * ( pow(s, 1/lambda) - 1 );
}

void TransportFlowEngine::particleVolumetricChange()
{
	Tesselation& Tes = solver->T[solver->currentTes];
	const Real                       suction0 = pAir - pZero;
	totalVolChange                            = 0;
	//FOREACH ( const shared_ptr<Body>& b, *scene->bodies )
	YADE_PARALLEL_FOREACH_BODY_BEGIN(const shared_ptr<Body>& b, scene->bodies)
	{
		if (!b) continue;
		Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh = Tes.vertex(b->getId());
		if (Vh->info().Pcondition or Vh==NULL) continue;
		const Real suction = pAir - Vh->info().potential;

		const Real volStrain = betam / alpham * (exp(-alpham * suction) - exp(-alpham * suction0));
		const Real vNew = Vh->info().vMixtureOriginal * (volStrain + 1.);
		const Real rNew = pow(3. * vNew / (4. * M_PI), 1. / 3.);
		//cout << "volstrain " << volStrain << " vNew " << vNew << " volorig " << Vh->info().vMixtureOriginal << " rnew " << rNew << endl;
		// if (rNew <= minParticleSwellFactor * Vh->info().radiiOriginal) continue;  // dont decrease size too much
		totalVolChange += (pow(rNew, 3.) - pow(Vh->point().weight(), 3.)) * 4. / 3. * M_PI;
		Vh->info().radiiChange = rNew - Vh->info().radiiOriginal;
		sphere->radius     = rNew;
		Vh->info().vMixture = pow(sphere->radius,3) * 4 / 3 * Mathr::PI; // vNew;
		Vh->info().vStrain = volStrain;
	}
	YADE_PARALLEL_FOREACH_BODY_END();
}

void TransportFlowEngine::cellVolumetricChange()
{
	Tesselation& Tes = solver->T[solver->currentTes];
	const Real                       suction0 = pAir - pZero;
	const long size  = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		if (cell->info().isFictious or cell->info().blocked or cell->info().Pcondition) continue;
		const Real suction = pAir - cell->info().potential;
		const Real volume = 1/cell->info().invVoidVolume();
		const Real volStrain = betam / alpham * (exp(-alpham * suction) - exp(-alpham * suction0));
		const Real swelledVol = cell->info().vMixtureOriginal * (volStrain + 1.);
		//cout << "suction0 " << suction0 << " suction " << suction << " volorigi " << cell->info().vMixtureOriginal << " vol " << swelledVol << endl;
		cell->info().vMixture = swelledVol < volume ? swelledVol : volume; // dont let it swell beyond the available volume
		cell->info().vMixtureChange = cell->info().vMixture - cell->info().vMixtureOriginal;
		cell->info().vStrain = volStrain;

	}
}

void TransportFlowEngine::computeSwellingPressure()
{
	Tesselation& Tes = solver->T[solver->currentTes];
	const long size  = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		if (cell->info().isFictious or cell->info().blocked or cell->info().Pcondition) continue;
		Real sumFacetFluidRatio = 0;
		std::for_each(cell->info().facetFluidSurfacesRatio.begin(), cell->info().facetFluidSurfacesRatio.end(), [&] (int n) {
    		sumFacetFluidRatio += n;
		});
		if (cell->info().vMixture < 1/cell->info().invVoidVolume() and sumFacetFluidRatio/4 < allowableFacetSphereRatioForFilled) {
			cell->info().swellingPressure = swellingPressureFactor * cell->info().potential * cell->info().saturation * ( cell->info().vMixture * cell->info().invVoidVolume() );
			// if (debug_precise) cout << "unfilled, swelling pressure " << cell->info().swellingPressure << endl;
			continue;
		} else if (cell->info().swellingPressure0 == 0 ) {
			cell->info().swellingPressure0 = cell->info().potential * cell->info().saturation ;
			cell->info().filled = true;
		}
		const Real swellingPressure = cell->info().potential * cell->info().saturation - cell->info().swellingPressure0;
		const Real maxSwellingPressure = getMaxSwellingPressure(cell);
		cell->info().swellingPressure = swellingPressureFactor * ( cell->info().potential * cell->info().saturation + std::min(maxSwellingPressure, swellingPressure) );
		// if (debug_precise) cout << "vol filled, swelling pressure " << cell->info().swellingPressure << endl;
	}

}

Real TransportFlowEngine::getMaxSwellingPressure(CellHandle& cell)
{
	const Real rhodb = cell->info().massSolids / cell->info().vMixture / 1000; // FIXME: convert from kg to Mg to use Wang 2012 parameters
	return 1e6 * (alphas * exp(betas * rhodb)); // FIXME: convert from MPa to Pa so we keep using Wang 2012 params
}

void TransportFlowEngine::updatePorosity(FlowSolver& flow)
{
	Tesselation& Tes = flow.T[flow.currentTes];
	const long size  = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		if (cell->info().Pcondition or cell->info().isFictious) continue;
		// if (!freezePorosity) {
		// 		Real poro = 1. - cell->info().vSolids / cell->info().vMixture ;
		// 		if (poro < minPoroClamp)
		// 			poro = minPoroClamp;
		// 		if (poro > maxPoroClamp)
		// 			poro = maxPoroClamp;
		// 		cell->info().porosity = poro;
		// }
		cell->info().porosity  = 1. - cell->info().vSolids / cell->info().vMixture ;
		cell->info().Po = Po * exp(a * (meanInitialPorosity - cell->info().porosity));
		cell->info().lambdao = lmbda * exp(b * (meanInitialPorosity - cell->info().porosity));
		//if (cell->info().lambdao < 0) cell->info().lambdao = 0;
	}

	if (!setParticlePorosity) return;
	YADE_PARALLEL_FOREACH_BODY_BEGIN(const shared_ptr<Body>& b, scene->bodies)
	{
		if (!b) continue;
		Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh = Tes.vertex(b->getId());
		if (Vh->info().Pcondition) continue;

		// if (!freezePorosity) {
		//
		// 		Real poro = 1. - Vh->info().vSolids / Vh->info().vMixture;
		// 		if (poro < minPoroClamp)
		// 			poro = minPoroClamp;
		// 		if (poro > maxPoroClamp)
		// 			poro = maxPoroClamp;
		// 		Vh->info().porosity = poro;
		// 	// }
		// }
		Vh->info().porosity = 1. - Vh->info().vSolids / Vh->info().vMixture;
		Vh->info().Po = Po * exp(a * (meanInitialPorosity - Vh->info().porosity));
		Vh->info().lambdao = lmbda * exp(this->b * (meanInitialPorosity - Vh->info().porosity));
		//if (Vh->info().lambdao < 0) Vh->info().lambdao = 0;

	}
	YADE_PARALLEL_FOREACH_BODY_END();
}

void TransportFlowEngine::updateParticleYoungsModulus()
{
	Tesselation&  Tes    = solver->T[solver->currentTes];
	const shared_ptr<InteractionContainer>& interactions = scene->interactions;
	const long                              size         = interactions->size();

	//FOREACH ( const shared_ptr<Body>& b, *scene->bodies )
	YADE_PARALLEL_FOREACH_BODY_BEGIN(const shared_ptr<Body>& b, scene->bodies)
	{
		if (!b) continue;
		Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		const auto mat1   = YADE_CAST<FrictMat*>(b->material.get());
		if (!sphere) continue;
		const VertexHandle& Vh = Tes.vertex(b->getId());
		if (Vh==NULL) continue;
		//const Real suction = pAir - Vh->info().potential;

		Real Ea = 3 * (1-2*mat1->poisson) * exp(alpham * -Vh->info().potential) / betam; // Darde 2021 uses suction (positive value) for this eqn
		//Real Eb = 3 * (1-2*mat2->poisson) * exp(alpham * -Vh2->info().potential) / betam;
		//if (Ea==0) cout << " zero youngs " << " pot1 " << -Vh1->info().potential << " pot 2 " << -Vh2->info().potential << endl;
		Ea = Ea > minYoung ? Ea : minYoung;
		Vh->info().youngs = Ea;
		//Ea = Ea < maxYoung ? Ea : maxYoung; Eb = Eb < maxYoung ? Eb : maxYoung;

	}
	YADE_PARALLEL_FOREACH_BODY_END();

#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		const shared_ptr<Interaction>& I = (*interactions)[i];
		const ScGeom* geom;
		if (!I || !I->geom.get() || !I->phys.get() || !I->isReal()) continue;
		if (I->geom.get()) {
			geom = YADE_CAST<ScGeom*>(I->geom.get());
			if (!geom) continue;

			auto mindlinphys = YADE_PTR_CAST<MindlinPhys>(I->phys);
			const shared_ptr<Body>&  b1 = (*scene->bodies)[I->getId1()];
			const shared_ptr<Body>&  b2 = (*scene->bodies)[I->getId2()];
			const auto mat1   = YADE_CAST<FrictMat*>(b1->material.get());
			const auto mat2   = YADE_CAST<FrictMat*>(b2->material.get());
			// FrictMat*  mat1 = dynamic_cast<FrictMat*>(b1_->material.get());
			// FrictMat*  mat2 = dynamic_cast<FrictMat*>(b2_->material.get());
			const VertexHandle& Vh1 = Tes.vertex(I->getId1());
			const VertexHandle& Vh2 = Tes.vertex(I->getId2());
			const Sphere* sphere1 = dynamic_cast<Sphere*>(b1->shape.get());
			const Sphere* sphere2 = dynamic_cast<Sphere*>(b1->shape.get());
			if (!sphere1 or !sphere2) continue;

			const Real Ea = Vh1->info().youngs;
			const Real Eb = Vh2->info().youngs;
			// Real Ea = 3 * (1-2*mat1->poisson) * exp(alpham * -Vh1->info().potential) / betam; // Darde 2021 uses suction (positive value) for this eqn
			// Real Eb = 3 * (1-2*mat2->poisson) * exp(alpham * -Vh2->info().potential) / betam;
			// if (Ea==0 or Eb==0) cout << " zero youngs " << " pot1 " << -Vh1->info().potential << " pot 2 " << -Vh2->info().potential << endl;
			// Ea = Ea > minYoung ? Ea : minYoung; Eb = Eb > minYoung ? Eb : minYoung;
			// Ea = Ea < maxYoung ? Ea : maxYoung; Eb = Eb < maxYoung ? Eb : maxYoung;
			//cout << " youngs mod a " << Ea << " youngs mod b " << Eb << " pot1 " << Vh1->info().potential << " pot2 " << Vh2->info().potential << " poisson " << mat1->poisson << endl;
			const Real E  = Ea * Eb / ((1. - math::pow(mat1->poisson, 2)) * Eb + (1. - math::pow(mat2->poisson, 2)) * Ea);
			const Real R = (sphere1->radius * sphere2->radius) / (sphere1->radius + sphere2->radius);
			const Real Ga = Ea / (2 * (1 + mat1->poisson));
			const Real Gb = Eb / (2 * (1 + mat2->poisson));
			const Real G  = (Ga + Gb) / 2;
                        const Real V = (mat1->poisson + mat2->poisson) / 2;
			const Real Ra = Ca * Ea;
			const Real Rb = Ca * Eb;
			mindlinphys->kno = 4. / 3. * E * sqrt(R);
			mindlinphys->kso = 2 * sqrt(4 * R) * G / (2 - V);
			mindlinphys->R = (Ra * Rb / (Ra + Rb));

			//Vh1->info().youngs = Ea;
			//Vh2->info().youngs = Eb;
		}
	}
}

void TransportFlowEngine::computeEquivalentBulkModuli(FlowSolver& flow)
{
	Tesselation& Tes = flow.T[flow.currentTes];
	const long size  = Tes.cellHandles.size();

#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell                   = Tes.cellHandles[i];
		const Real        waterFrac              = (cell->info().saturation * cell->info().porosity) / Kw;
		const Real        airFrac                = cell->info().porosity * (1. - cell->info().saturation) / Ka;
		const Real        solidFrac              = (1. - cell->info().porosity) / Ks;
		const Real        Keq                    = 1 / (waterFrac + airFrac + solidFrac);
		cell->info().equivalentBulkModulus = Keq;
	}

	YADE_PARALLEL_FOREACH_BODY_BEGIN (const auto& b, scene->bodies) {
		if (!b) continue;
		const Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh = Tes.vertex(b->getId());
		if (Vh->info().isFictious or Vh->info().Pcondition) continue;

		const Real        waterFrac              = (Vh->info().saturation * Vh->info().porosity) / Kw;
		const Real        airFrac                = Vh->info().porosity * (1. - Vh->info().saturation) / Ka;
		const Real        solidFrac              = (1. - Vh->info().porosity) / Ks;
		const Real        Keq                    = 1 / (waterFrac + airFrac + solidFrac);
		Vh->info().equivalentBulkModulus = Keq;
	}
	YADE_PARALLEL_FOREACH_BODY_END();
}


void TransportFlowEngine::updateForces()
{
	computeFacetForcesWithCache(false);
	scene->forces.sync();
	applySwellingForces(*solver);
}



void TransportFlowEngine::unboundCavityParticles()
{
	YADE_PARALLEL_FOREACH_BODY_BEGIN(const shared_ptr<Body>& b, scene->bodies)
	{
		if (b->shape->getClassIndex() != Sphere::getClassIndexStatic() || !b) continue;
		auto* thState = b->state.get();
		if (thState->isCavity) {
			b->setBounded(false);
			if (debug) cout << "cavity body unbounded" << endl;
		}
	}
	YADE_PARALLEL_FOREACH_BODY_END();
}

void TransportFlowEngine::setInitialMixtureVolumes(FlowSolver& flow)
{
	Tesselation& Tes = flow.T[flow.currentTes];
	const long size = Tes.cellHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];

		Real fracMixture;
		if (!homogeneousMixtureVolumes) {
			fracMixture = initialCellMixtureFraction * weibullDeviate(lambdaWeibullShape, kappaWeibullScale);
			if (fracMixture < 0)
				fracMixture = 0;
			if (fracMixture > 1)
				fracMixture = 1;
		} else {
			fracMixture = initialCellMixtureFraction;
		}

		cell->info().porosity = meanInitialPorosity;
		cell->info().Po = Po * exp(a * (meanInitialPorosity - cell->info().porosity));
		cell->info().lambdao = lmbda * exp(b * (meanInitialPorosity - cell->info().porosity));
		cell->info().saturation = vanGenuchten(cell, -cell->info().potential);
		cell->info().vMixture = fracMixture / cell->info().invVoidVolume();
		cell->info().vMixtureOriginal = cell->info().vMixture;
		cell->info().vSolids = (1 - cell->info().porosity) * cell->info().vMixtureOriginal;
		cell->info().massWater = (cell->info().saturation * (cell->info().vMixture - cell->info().vSolids)) * rho;
		//cell->info().vWater = cell->info().massWater / rho;
	}
}

void TransportFlowEngine::setInitialPorosity(FlowSolver& flow)
{ // assume that the porosity is weibull distributed
	Tesselation& Tes = flow.T[flow.currentTes];
	//const long size = Tes.cellHandles.size();
// #ifdef YADE_OPENMP
// #pragma omp parallel for
// #endif
// 	for (long i = 0; i < size; i++) {
// 		CellHandle& cell = Tes.cellHandles[i];
//
// 		if (!resetVolumeSolids) {
// 			Real poro;
// 			if (!homogeneousPorosity) {
// 				poro = meanInitialPorosity * weibullDeviate(lambdaWeibullShape, kappaWeibullScale);
// 				if (poro < minPoroClamp)
// 					poro = minPoroClamp;
// 				if (poro > maxPoroClamp)
// 					poro = maxPoroClamp;
// 			} else {
// 				poro = meanInitialPorosity;
// 			}
// 			cell->info().porosity = cell->info().initialPorosity = poro;
// 			if (poro > maxPorosity)
// 				maxPorosity = poro;
// 		}
// 		//cell->info().vSolids = (1 - cell->info().porosity) * cell->info().vMixtureOriginal;
// 		//cell->info().massSolids = cell->info().vSolids * rho_solid;
// 		if (!resetVolumeSolids) {
// 			cell->info().Po = Po * exp(a * (meanInitialPorosity - cell->info().porosity));
// 			cell->info().lambdao = lmbda * exp(b * (meanInitialPorosity - cell->info().porosity));
// 		}
// 	}


	//if (setParticlePorosity)
	//{
	YADE_PARALLEL_FOREACH_BODY_BEGIN(const shared_ptr<Body>& b, scene->bodies)
	{
		if (!b) continue;
		Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh = Tes.vertex(b->getId());
		if (Vh->info().Pcondition) continue;

		// if (!resetVolumeSolids) {
		// 	Real poro;
		// 	if (!homogeneousPorosity) {
		// 		poro = meanInitialPorosity * weibullDeviate(lambdaWeibullShape, kappaWeibullScale);
		// 		if (poro < minPoroClamp)
		// 			poro = minPoroClamp;
		// 		if (poro > maxPoroClamp)
		// 			poro = maxPoroClamp;
		// 	} else {
		// 		poro = meanInitialPorosity;
		// 	}
		// 	Vh->info().porosity = Vh->info().initialPorosity = poro;
		// 	if (poro > maxPorosity)
		// 		maxPorosity = poro;
		// }

		Vh->info().porosity = meanInitialPorosity;
		Vh->info().Po = Po * exp(a * (meanInitialPorosity - Vh->info().porosity));
		Vh->info().lambdao = lmbda * exp(this->b * (meanInitialPorosity - Vh->info().porosity));
		Vh->info().vSolids =  (1 - Vh->info().porosity) * Vh->info().vMixtureOriginal;
		Vh->info().saturation = vanGenuchten(Vh, -Vh->info().potential);
		Vh->info().massWater = (Vh->info().saturation * (Vh->info().vMixture - Vh->info().vSolids)) * rho;



	}
	YADE_PARALLEL_FOREACH_BODY_END();
	//}

	if (resetVolumeSolids)
		resetVolumeSolids = false;
}

Real TransportFlowEngine::weibullDeviate(Real lambda, Real k)
{
	std::random_device              rd;
	std::mt19937                    e2(rd());
	std::weibull_distribution<Real> weibullDistribution(lambda, k);
	Real                            correction = weibullDistribution(e2);
	return correction;
}



// VISUALIZATION TOOLS //

#ifdef YADE_VTK
void TransportFlowEngine::saveVertexInfoVTK(string fileName)
{
	vtkSmartPointer<vtkPointsReal>              spheresPos = vtkSmartPointer<vtkPointsReal>::New();
	vtkSmartPointer<vtkCellArray>  		    spheresCells = vtkSmartPointer<vtkCellArray>::New();

	vtkSmartPointer<vtkDoubleArrayFromReal> radii = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	radii->SetNumberOfComponents(1);
	radii->SetName("radii");

	vtkSmartPointer<vtkDoubleArrayFromReal> radiiChange = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	radiiChange->SetNumberOfComponents(1);
	radiiChange->SetName("radiiChange");

	vtkSmartPointer<vtkDoubleArrayFromReal> spheresId = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	spheresId->SetNumberOfComponents(1);
	spheresId->SetName("id");

	vtkSmartPointer<vtkDoubleArrayFromReal> potential = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	potential->SetNumberOfComponents(1);
	potential->SetName("potential");

	vtkSmartPointer<vtkDoubleArrayFromReal> saturation = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	saturation->SetNumberOfComponents(1);
	saturation->SetName("saturation");

	vtkSmartPointer<vtkDoubleArrayFromReal> massWater = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	massWater->SetNumberOfComponents(1);
	massWater->SetName("massWater");

	vtkSmartPointer<vtkDoubleArrayFromReal> youngs = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	youngs->SetNumberOfComponents(1);
	youngs->SetName("youngs");

	vtkSmartPointer<vtkDoubleArrayFromReal> vStrain = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	vStrain->SetNumberOfComponents(1);
	vStrain->SetName("vStrain");

	vtkSmartPointer<vtkDoubleArrayFromReal> conductivity = vtkSmartPointer<vtkDoubleArrayFromReal>::New();
	conductivity->SetNumberOfComponents(1);
	conductivity->SetName("conductivity");

	// cout << "set up constants in savevertex" << endl;

	Tesselation& Tes = solver->T[solver->currentTes];

	for (const auto& b : *scene->bodies) {
		if (!b) continue;
		const Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		const VertexHandle& Vh1 = Tes.vertex(b->getId());
		if (sphere) {
			vtkIdType pid[1];
			Vector3r  pos(b->state->pos);
			pid[0] = spheresPos->InsertNextPoint(pos);
			//cout << "insert pos" << endl;
			spheresCells->InsertNextCell(1, pid);
			radii->InsertNextValue(sphere->radius);
			radiiChange->InsertNextValue(Vh1->info().radiiChange);
			//cout << "get potential" << endl;
			potential->InsertNextValue(Vh1->info().potential);
			saturation->InsertNextValue(Vh1->info().saturation);
			massWater->InsertNextValue(Vh1->info().massWater);
			youngs->InsertNextValue(Vh1->info().youngs);
			vStrain->InsertNextValue(Vh1->info().vStrain);
			conductivity->InsertNextValue(Vh1->info().conductivity);
			spheresId->InsertNextValue(b->getId());
		}

	}

	//cout << "ready to write everything" << endl;
	vtkSmartPointer<vtkUnstructuredGrid> spheresUg = vtkSmartPointer<vtkUnstructuredGrid>::New();
	spheresUg->SetPoints(spheresPos);
	spheresUg->SetCells(VTK_VERTEX, spheresCells);
	spheresUg->GetPointData()->AddArray(radii);
	spheresUg->GetPointData()->AddArray(radiiChange);
	spheresUg->GetPointData()->AddArray(potential);
	spheresUg->GetPointData()->AddArray(saturation);
	spheresUg->GetPointData()->AddArray(massWater);
	spheresUg->GetPointData()->AddArray(youngs);
	spheresUg->GetPointData()->AddArray(vStrain);
	spheresUg->GetPointData()->AddArray(conductivity);
	spheresUg->GetPointData()->AddArray(spheresId);

	vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
	string fn = fileName + "spheres." + boost::lexical_cast<string>(scene->iter) + ".vtu";
	writer->SetFileName(fn.c_str());
	writer->SetInputData(spheresUg);
	writer->Write();
}


void TransportFlowEngine::saveCellInfoVTK(const char* folder, bool withBoundaries)
{
	vector<int>
	            allIds; //an ordered list of cell ids (from begin() to end(), for vtk table lookup), some ids will appear multiple times since boundary cells are split into multiple tetrahedra
	vector<int> fictiousN;
	bool        initNoCache = solver->noCache;
	solver->noCache         = false;

	static unsigned int number = 0;
	char                filename[250];
	mkdir(folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	sprintf(filename, "%s/out_%d.vtk", folder, number++);
	basicVTKwritter vtkfile(0, 0);
	solver->saveMesh(vtkfile, withBoundaries, allIds, fictiousN, filename);
	solver->noCache = initNoCache;

	vtkfile.begin_data("Potential", CELL_DATA, SCALARS, FLOAT);
	for (unsigned kk = 0; kk < allIds.size(); kk++)
		vtkfile.write_data(solver->tesselation().cellHandles[allIds[kk]]->info().potential);
	vtkfile.end_data();

	vtkfile.begin_data("Porosity", CELL_DATA, SCALARS, FLOAT);
	for (unsigned kk = 0; kk < allIds.size(); kk++)
		vtkfile.write_data(solver->tesselation().cellHandles[allIds[kk]]->info().porosity);
	vtkfile.end_data();
	//
	vtkfile.begin_data("Saturation", CELL_DATA, SCALARS, FLOAT);
	for (unsigned kk = 0; kk < allIds.size(); kk++)
		vtkfile.write_data(solver->tesselation().cellHandles[allIds[kk]]->info().saturation);
	vtkfile.end_data();

	vtkfile.begin_data("Pcondition", CELL_DATA, SCALARS, FLOAT);
	for (unsigned kk = 0; kk < allIds.size(); kk++)
		vtkfile.write_data(solver->tesselation().cellHandles[allIds[kk]]->info().Pcondition);
	vtkfile.end_data();

	vtkfile.begin_data("filled", CELL_DATA, SCALARS, FLOAT);
	for (unsigned kk = 0; kk < allIds.size(); kk++)
		vtkfile.write_data(solver->tesselation().cellHandles[allIds[kk]]->info().filled);
	vtkfile.end_data();

	vtkfile.begin_data("isFictious", CELL_DATA, SCALARS, FLOAT);
	for (unsigned kk = 0; kk < allIds.size(); kk++)
		vtkfile.write_data(solver->tesselation().cellHandles[allIds[kk]]->info().isFictious);
	vtkfile.end_data();

	averageRelativeCellVelocity_transport();
	vtkfile.begin_data("Velocity", CELL_DATA, VECTORS, FLOAT);
	for (unsigned kk = 0; kk < allIds.size(); kk++)
		vtkfile.write_data(
		        solver->tesselation().cellHandles[allIds[kk]]->info().averageVelocity()[0],
		        solver->tesselation().cellHandles[allIds[kk]]->info().averageVelocity()[1],
		        solver->tesselation().cellHandles[allIds[kk]]->info().averageVelocity()[2]);
	vtkfile.end_data();

#define SAVE_CELL_INFO(INFO)                                                                                                                                   \
	vtkfile.begin_data(#INFO, CELL_DATA, SCALARS, FLOAT);                                                                                                  \
	for (unsigned kk = 0; kk < allIds.size(); kk++)                                                                                                        \
		vtkfile.write_data(solver->tesselation().cellHandles[allIds[kk]]->info().INFO);                                                                \
	vtkfile.end_data();
 	SAVE_CELL_INFO(massWater)
	SAVE_CELL_INFO(Po)
	SAVE_CELL_INFO(lambdao)
	SAVE_CELL_INFO(Pcondition)
	SAVE_CELL_INFO(vMixture)
	SAVE_CELL_INFO(vMixtureChange)
	SAVE_CELL_INFO(swellingPressure)
	SAVE_CELL_INFO(vStrain)
	SAVE_CELL_INFO(conductivity)
}
#endif



Real TransportFlowEngine::getCellPotential(Vector3r pos)
{
	RTriangulation& Tri = solver->T[solver->currentTes].Triangulation();
	CellHandle      cell = Tri.locate(CGT::Sphere(pos[0], pos[1], pos[2]));
	return cell->info().potential;
}

Real TransportFlowEngine::getVertexPotential(const int id)
{
	Tesselation& Tes = solver->T[solver->currentTes];
	const VertexHandle& Vh1 = Tes.vertex(id);
	return Vh1->info().potential;
}

void TransportFlowEngine::interpolateVertices(Tesselation& Tes, Tesselation& NewTes)
{
	const long size = NewTes.vertexHandles.size();
#ifdef YADE_OPENMP
#pragma omp parallel for
#endif
	for (long v = 0; v < size; v++) {
		// CVector newPoint(0,0,0);
		VertexHandle& Vh = NewTes.vertexHandles[v];
		if (Vh == NULL) continue;

		// CVector newPoint(Vh->point().point()[0],Vh->point().point()[1],Vh->point().point()[2]);
		// const VertexHandle& oldVh = Tes.findNearestVertex(newPoint);

		const int vid = Vh->info().id();
		const VertexHandle& oldVh = Tes.vertex(vid);

		//if (debug_precise) cout << endl << "found nearest vertex " << oldVh->info().id() << " for new vertex " << Vh->info().id() << endl << endl;
		Vh->info().vMixtureOriginal = oldVh->info().vMixtureOriginal;
		Vh->info().radiiOriginal = oldVh->info().radiiOriginal;
		if (!Vh->info().Pcondition) Vh->info().potential = oldVh->info().potential;
		Vh->info().Po = oldVh->info().Po;
		Vh->info().lambdao = oldVh->info().lambdao ;
		Vh->info().oldPotential = oldVh->info().oldPotential;
		Vh->info().conductivity = oldVh->info().conductivity;
		//Vh->info().dsdp = oldVh->info().dsdp;
		Vh->info().massWater = oldVh->info().massWater;// FIXME do not interpolate intrinsic variables
		Vh->info().oldMassWater = oldVh->info().oldMassWater;
		Vh->info().porosity = oldVh->info().porosity;
		// Vh->info().vMixture  = oldVh->info().vMixture;
		if (!Vh->info().Pcondition) Vh->info().saturation = oldVh->info().saturation;
		Vh->info().initialSaturation = oldVh->info().initialSaturation;
		Vh->info().initialPorosity   = oldVh->info().initialPorosity;
		Vh->info().vSolids   = oldVh->info().vSolids;
		Vh->info().vStrain   = oldVh->info().vStrain;
		Vh->info().vMixture   = oldVh->info().vMixture;
		Vh->info().isFictious   = oldVh->info().isFictious;
		Vh->info().fluxSum   = oldVh->info().fluxSum;

		//if ( debug ) cout << endl << "interpolated all the stuff for vertex " << Vh->info().id() << endl << endl;
		Vh->info().setId(oldVh->info().id());

	}
}

void TransportFlowEngine::interpolateTesselations(Tesselation& Tes, Tesselation& NewTes, Solver& flow)
{
	CellHandle      oldCell;
	VertexHandle 	oldVertex;
	RTriangulation& Tri = Tes.Triangulation();
	//#ifdef YADE_OPENMP
	const long size = NewTes.cellHandles.size();
	//#pragma omp parallel for num_threads(ompThreads>0 ? ompThreads : 1)
	for (long i2 = 0; i2 < size; i2++) {
		CellHandle& newCell = NewTes.cellHandles[i2];
		if (newCell->info().isGhost || newCell->info().isAlpha) continue; // a new alpha cell will have vertices that are not in the original Tes.vertex().
		CVector center(0, 0, 0);
		//cout <<"get center of alpha cell? " << newCell->info().isAlpha << endl;
		if (newCell->info().fictious() == 0)
			for (int k = 0; k < 4; k++) {
				center = center + 0.25 * (Tes.vertex(newCell->vertex(k)->info().id())->point().point() - CGAL::ORIGIN);
			}
		else {
			Real boundPos = 0;
			int  coord    = 0;
			for (int k = 0; k < 4; k++) {
				if (!newCell->vertex(k)->info().isFictious)
					center = center
					        + (1. / (4. - newCell->info().fictious()))
					                * (Tes.vertex(newCell->vertex(k)->info().id())->point().point() - CGAL::ORIGIN);
			}
			for (int k = 0; k < 4; k++) {
				if (newCell->vertex(k)->info().isFictious) {
					coord    = flow.cellTransferBoundary(newCell->vertex(k)->info().id()).coordinate;
					boundPos = flow.cellTransferBoundary(newCell->vertex(k)->info().id()).p[coord];
					center   = CVector(
                                                coord == 0 ? boundPos : center[0],
                                                coord == 1 ? boundPos : center[1],
                                                coord == 2 ? boundPos : center[2]);
				}
			}
		}
		oldCell = Tri.locate(CGT::Sphere(center[0], center[1], center[2]));
		if (!newCell->info().Pcondition) newCell->info().potential = oldCell->info().potential;
		if (!newCell->info().Pcondition) newCell->info().saturation = oldCell->info().saturation;
		newCell->info().initialSaturation = oldCell->info().initialSaturation;
		newCell->info().initialPorosity   = oldCell->info().initialPorosity;

		newCell->info().porosity = oldCell->info().porosity;
		newCell->info().vMixture  = oldCell->info().vMixture;
		newCell->info().vMixtureOriginal  = oldCell->info().vMixtureOriginal;
		newCell->info().vMixtureChange  = oldCell->info().vMixtureChange;
		newCell->info().blocked = oldCell->info().blocked;
		//newCell->info().dsdp = oldCell->info().dsdp;
		newCell->info().Po = oldCell->info().Po;
		newCell->info().lambdao = oldCell->info().lambdao;
		newCell->info().oldPotential = oldCell->info().oldPotential;
		newCell->info().conductivity = oldCell->info().conductivity;
		newCell->info().massWater = oldCell->info().massWater;
		newCell->info().vSolids = oldCell->info().vSolids;
		newCell->info().vWater = oldCell->info().vWater;
		newCell->info().vStrain = oldCell->info().vStrain;
		newCell->info().filled = oldCell->info().filled;
		newCell->info().massSolids = oldCell->info().massSolids;
		newCell->info().swellingPressure0 = oldCell->info().swellingPressure0;
		newCell->info().swellingPressure = oldCell->info().swellingPressure;
		newCell->info().initialDryDensity = oldCell->info().initialDryDensity;
	}
}
//
// void TransportFlowEngine::computeFacetForcesWithCache(bool onlyCache)
// 	{
// 		// RTriangulation& Tri = T[currentTes].Triangulation
// 		RTriangulation& Tri = solver->T[solver->currentTes].Triangulation();
// 		CVector         nullVect(0, 0, 0);
// 		//reset forces
// 		if (!onlyCache)
// 			for (FiniteVerticesIterator v = Tri.finite_vertices_begin(); v != Tri.finite_vertices_end(); ++v)
// 				v->info().forces = nullVect;
//
// 		/*	#ifdef parallel_forces*/
// 		/*	if (noCache) {*/
// 		/*		perVertexUnitForce.clear(); perVertexPressure.clear();*/
// 		/*		perVertexUnitForce.resize(T[currentTes].maxId+1);*/
// 		/*		perVertexPressure.resize(T[currentTes].maxId+1);}*/
// 		/*	#endif*/
// 		CellHandle   neighbourCell;
// 		VertexHandle mirrorVertex;
// 		CVector      tempVect;
// 		//FIXME : Ema, be carefull with this (noCache), it needs to be turned true after retriangulation
// 		if (noCache) {
// 			for (VCellIterator cellIt = solver->T[solver->currentTes].cellHandles.begin(); cellIt != solver->T[solver->currentTes].cellHandles.end(); cellIt++) {
// 				CellHandle& cell = *cellIt;
// 				//reset cache
// 				for (int k = 0; k < 4; k++) cell->info().unitForceVectors[k] = nullVect;
//
// 				for (int j = 0; j < 4; j++)
// 					if (!Tri.is_infinite(cell->neighbor(j))) {
// 						neighbourCell        = cell->neighbor(j);
// 						const CVector& Surfk = cell->info().facetSurfaces[j];
// 						//FIXME : later compute that fluidSurf only once in hydraulicRadius, for now keep full surface not modified in cell->info for comparison with other forces schemes
// 						//The ratio void surface / facet surface
// 						//Area of the facet (i.e. the triangle)
// 						Real area = sqrt(Surfk.squared_length());
// 						if (area <= 0) cerr << "AREA <= 0!!" << endl;
// 						CVector                     facetNormal   = Surfk / area;
// 						const std::vector<CVector>& crossSections = cell->info().facetSphereCrossSections;
// 						//This is the cross-sectional area of the throat
// 						CVector fluidSurfk = cell->info().facetSurfaces[j] * cell->info().facetFluidSurfacesRatio[j];
// 						/// handle fictious vertex since we can get the projected surface easily here
// 						if (cell->vertex(j)->info().isFictious and !cell->info().isAlpha) {
// 							//projection of facet on the boundary
// 							Real projSurf                  = std::abs(Surfk[solver->cellTransferBoundary(cell->vertex(j)->info().id()).coordinate]);
// 							tempVect                       = -projSurf * solver->cellTransferBoundary(cell->vertex(j)->info().id()).normal;
// 							cell->vertex(j)->info().forces = cell->vertex(j)->info().forces + tempVect * cell->info().swellingPressure;
// 							//define the cached value for later use with cache*p
// 							cell->info().unitForceVectors[j] = cell->info().unitForceVectors[j] + tempVect;
// 						}
// 						/// Apply weighted forces f_k=sqRad_k/sumSqRad*f
// 						CVector facetUnitForce = -fluidSurfk * cell->info().solidSurfaces[j][3];
// 						CVector facetForce;
// 						facetForce = cell->info().swellingPressure * facetUnitForce;
//
// 						for (int y = 0; y < 3; y++) {
// 							//1st the drag (viscous) force weighted by surface of spheres in the throat
// 							cell->vertex(facetVertices[j][y])->info().forces = cell->vertex(facetVertices[j][y])->info().forces
// 							        + facetForce * cell->info().solidSurfaces[j][y]; // no viscous drag in partiallysaturated clay
// 							//(add to cached value)
// 							cell->info().unitForceVectors[facetVertices[j][y]] = cell->info().unitForceVectors[facetVertices[j][y]]
// 							        + facetUnitForce * cell->info().solidSurfaces[j][y];
// 							//2nd the partial integral of pore pressure, which boils down to weighting by partial cross-sectional area
// 							//uncomment to get total force / comment to get only viscous forces (Bruno)
// 							if (!cell->vertex(facetVertices[j][y])->info().isFictious) {
// 								cell->vertex(facetVertices[j][y])->info().forces = cell->vertex(facetVertices[j][y])->info().forces
// 									        - facetNormal * cell->info().swellingPressure * crossSections[j][y]; // when
// 								cell->info().unitForceVectors[facetVertices[j][y]]
// 								        = cell->info().unitForceVectors[facetVertices[j][y]]
// 								        - facetNormal * crossSections[j][y];
// 							}
// 						}
// 						/*					#ifdef parallel_forces*/
// 						/*					perVertexUnitForce[cell->vertex(j)->info().id()].push_back(&(cell->info().unitForceVectors[j]));*/
// 						/*					if  ((cell->info().isFictious && freeSwelling) || cell->info().isExposed) perVertexPressure[cell->vertex(j)->info().id()].push_back(&(pAir));*/
// 						/*					else perVertexPressure[cell->vertex(j)->info().id()].push_back(&(cell->info().swellingPressure));*/
// 						/*					#endif*/
// 					}
// 			}
// 			noCache = false; //cache should always be defined after execution of this function
// 			if (onlyCache)
// 				return;
// 		} else { //use cached values
//
// 			//		#ifndef parallel_forces
// 			//	#pragma omp parallel for
// 			for (FiniteCellsIterator cell = Tri.finite_cells_begin(); cell != Tri.finite_cells_end(); cell++) {
// 				for (int yy = 0; yy < 4; yy++) {
// 					if (0){ //(((cell->info().isFictious or cell->info().isAlpha) && freeSwelling) /*|| cell->info().isExposed*/) {
// 						cell->vertex(yy)->info().forces = cell->vertex(yy)->info().forces + cell->info().unitForceVectors[yy] * pAir;
// 					} else {
// 						cell->vertex(yy)->info().forces = cell->vertex(yy)->info().forces
// 						        + cell->info().unitForceVectors[yy] * cell->info().swellingPressure;
// 						// if (useKeq) {
// 						// 	Real delP = -cell->info().dv() * cell->info().equivalentBulkModulus
// 						// 	        / cell->info().volume(); // assume that each step starts with 0facet force due to volume changes (if there was no volume change, then there should be no force due to compressible mixture in cell)
// 						// 	cell->vertex(yy)->info().forces
// 						// 	        = cell->vertex(yy)->info().forces + cell->info().unitForceVectors[yy] * delP;
// 						// }
// 					}
// 				}
// 			}
//
// 			/*		#else*/
// 			/*		#pragma omp parallel for num_threads(ompThreads)*/
// 			/*		for (int vn=0; vn<= T[currentTes].maxId; vn++) {*/
// 			/*			if (T[currentTes].vertexHandles[vn]==NULL) continue;*/
// 			/*			VertexHandle& v = T[currentTes].vertexHandles[vn];*/
// 			/*			const int& id =  v->info().id();*/
// 			/*			CVector tf (0,0,0);*/
// 			/*			int k=0;*/
// 			/*			for (vector<const Real*>::iterator c = perVertexPressure[id].begin(); c != perVertexPressure[id].end(); c++)*/
// 			/*				tf = tf + (*(perVertexUnitForce[id][k++]))*(**c);*/
// 			/*			v->info().forces = tf;*/
// 			/*		}*/
// 			/*		#endif*/
// 		}
// 		if (debug) {
// 			CVector totalForce = nullVect;
// 			for (FiniteVerticesIterator v = Tri.finite_vertices_begin(); v != Tri.finite_vertices_end(); ++v) {
// 				if (!v->info().isFictious)
// 					totalForce = totalForce + v->info().forces;
// 				else if (solver->cellTransferBoundary(v->info().id()).fluxCondition == 1)
// 					totalForce = totalForce + v->info().forces;
// 			}
// 			cout << "totalForce = " << totalForce << endl;
// 		}
// 	}
//
//


void TransportFlowEngine::computeFacetForcesWithCache(bool onlyCache)
{
	RTriangulation& Tri = solver->T[solver->currentTes].Triangulation();
	Tesselation& Tes = solver->T[solver->currentTes];
	CVector         nullVect(0, 0, 0);
	//reset forces
	if (!onlyCache)
		for (FiniteVerticesIterator v = Tri.finite_vertices_begin(); v != Tri.finite_vertices_end(); ++v)
			v->info().forces = nullVect;

// #ifdef parallel_forces
// 	if (solver->noCache) {
// 		perVertexUnitForce.clear();
// 		perVertexPressure.clear();
// 		perVertexUnitForce.resize(solver->T[solver->currentTes].maxId + 1);
// 		perVertexPressure.resize(solver->T[solver->currentTes].maxId + 1);
// 	}
// #endif
	CellHandle   neighbourCell;
	VertexHandle mirrorVertex;
	CVector      tempVect;
	//FIXME : Ema, be carefull with this (noCache), it needs to be turned true after retriangulation
	if (solver->noCache) {
		for (VCellIterator cellIt = solver->T[solver->currentTes].cellHandles.begin(); cellIt != solver->T[solver->currentTes].cellHandles.end(); cellIt++) {
			CellHandle& cell = *cellIt;
			//reset cache
			for (int k = 0; k < 4; k++)
				cell->info().unitForceVectors[k] = nullVect;

			for (int j = 0; j < 4; j++)
				if (!Tri.is_infinite(cell->neighbor(j))) {
					neighbourCell = cell->neighbor(j);
					const CVector& Surfk = cell->info().facetSurfaces[j];
					//FIXME : later compute that fluidSurf only once in hydraulicRadius, for now keep full surface not modified in cell->info for comparison with other forces schemes
					//The ratio void surface / facet surface
					//Area of the facet (i.e. the triangle)
					Real area = sqrt(Surfk.squared_length());
					if (area <= 0) {
						cerr << "AREA <= 0!!" << endl;
						continue;;
					}

					CVector                     facetNormal = Surfk / area;
					const std::vector<CVector>& crossSections = cell->info().facetSphereCrossSections;
					//This is the cross-sectional area of the throat
					CVector fluidSurfk = cell->info().facetSurfaces[j] * cell->info().facetFluidSurfacesRatio[j];
					/// handle fictious vertex since we can get the projected surface easily here
					if (cell->vertex(j)->info().isFictious) {
						//projection of facet on the boundary
						Real projSurf = math::abs(Surfk[solver->cellTransferBoundary(cell->vertex(j)->info().id()).coordinate]);
						tempVect = -projSurf * solver->cellTransferBoundary(cell->vertex(j)->info().id()).normal;
						cell->vertex(j)->info().forces = cell->vertex(j)->info().forces + tempVect * cell->info().swellingPressure;
						//define the cached value for later use with cache*p
						cell->info().unitForceVectors[j] = cell->info().unitForceVectors[j] + tempVect;
					}
					/// Apply weighted forces f_k=sqRad_k/sumSqRad*f
					CVector facetUnitForce = -fluidSurfk * cell->info().solidSurfaces[j][3];
					CVector facetForce = cell->info().swellingPressure * facetUnitForce;
					//if (debug_precise) cout << "facet force " << facetForce << " swelling pressure " << cell->info().swellingPressure << " fluidSurfK " << fluidSurfk << " solid surface " << cell->info().solidSurfaces[j][3] << endl;
					for (int y = 0; y < 3; y++) {
						//1st the drag (viscous) force weighted by surface of spheres in the throat
						cell->vertex(facetVertices[j][y])->info().forces = cell->vertex(facetVertices[j][y])->info().forces
						        + facetForce * cell->info().solidSurfaces[j][y];
						//(add to cached value)
						cell->info().unitForceVectors[facetVertices[j][y]] = cell->info().unitForceVectors[facetVertices[j][y]]
						        + facetUnitForce * cell->info().solidSurfaces[j][y];
						//2nd the partial integral of pore pressure, which boils down to weighting by partial cross-sectional area
						//uncomment to get total force / comment to get only viscous forces (Bruno)
						if (!cell->vertex(facetVertices[j][y])->info().isFictious) {
							cell->vertex(facetVertices[j][y])->info().forces
							        = cell->vertex(facetVertices[j][y])->info().forces
							        - facetNormal * cell->info().swellingPressure * crossSections[j][y];
							//add to cached value
							cell->info().unitForceVectors[facetVertices[j][y]]
							        = cell->info().unitForceVectors[facetVertices[j][y]]
							        - facetNormal * crossSections[j][y];
						}
					}
// #ifdef parallel_forces
// 					perVertexUnitForce[cell->vertex(j)->info().id()].push_back(&(cell->info().unitForceVectors[j]));
// 					perVertexPressure[cell->vertex(j)->info().id()].push_back(&(cell->info().swellingPressure));
// #endif
				}
		}
		solver->noCache = false; //cache should always be defined after execution of this function
	}
	if (onlyCache) return;
		// 	} else {//use cached values
//#ifndef parallel_forces
	const long size = Tes.cellHandles.size();
// #ifdef YADE_OPENMP
// #pragma omp parallel for
// #endif
	for (long i = 0; i < size; i++) {
		CellHandle& cell = Tes.cellHandles[i];
		for (int yy = 0; yy < 4; yy++) cell->vertex(yy)->info().forces = cell->vertex(yy)->info().forces + cell->info().unitForceVectors[yy] * cell->info().swellingPressure;
	// for (FiniteCellsIterator cell = Tri.finite_cells_begin(); cell != cellEnd; cell++) {
	// 	for (int yy = 0; yy < 4; yy++)
	// 		cell->vertex(yy)->info().forces = cell->vertex(yy)->info().forces + cell->info().unitForceVectors[yy] * cell->info().swellingPressure;
	}

// #else
// #ifdef YADE_OPENMP
// #pragma omp parallel
// #endif
// 	for (int vn = 0; vn <= solver->T[solver->currentTes].maxId; vn++) {
// 		if (solver->T[solver->currentTes].vertexHandles[vn] == NULL) continue;
// 		VertexHandle& v = solver->T[solver->currentTes].vertexHandles[vn];
// 		const int&    id = v->info().id();
// 		CVector       tf(0, 0, 0);
// 		int           k = 0;
// 		for (vector<const Real*>::iterator c = perVertexPressure[id].begin(); c != perVertexPressure[id].end(); c++)
// 			tf = tf + (*(perVertexUnitForce[id][k++])) * (**c);
// 		v->info().forces = tf;
// 	}
// #endif
	// 	}
	if (debug) {
		CVector totalForce = nullVect;
		for (FiniteVerticesIterator v = Tri.finite_vertices_begin(); v != Tri.finite_vertices_end(); ++v) {
			if (!v->info().isFictious) totalForce = totalForce + v->info().forces;
			// else if (solver->cellTransferBoundary(v->info().id()).fluxCondition == 1)
			// 	totalForce = totalForce + v->info().forces;
		}
		cout << "totalForce = " << totalForce << endl;
	}
}

void TransportFlowEngine::applySwellingForces(Solver& /*flow*/)
{

	Tesselation& Tes = solver->T[solver->currentTes];
	Vector3r force;
	for (const auto& b : *scene->bodies) {
		if (!b) continue;
		const Sphere* sphere = dynamic_cast<Sphere*>(b->shape.get());
		if (!sphere) continue;
		const VertexHandle& Vh1 = Tes.vertex(b->getId());
		if (Vh1 == NULL) continue;
		force = Vector3r ( Vh1->info().forces[0],Vh1->info().forces[1],Vh1->info().forces[2] );
		scene->forces.addForce ( Vh1->info().id(), force);
		//if (debug_precise) cout << "force added " << force << endl;
	}

}

void TransportFlowEngine::setVolumeFractionWithImageryGrid(string imageryFilePath2, FlowSolver& flow)
{
	std::ifstream file;
	file.open(imageryFilePath2);
	if (!file) {
		cerr << "Unable to open imagery grid reverting to weibull volumefraction distribution" << endl;
		setInitialMixtureVolumes(flow);
		return;
	}
	std::vector<Vector3r> gridCoords;
	std::vector<Real>     porosities;
	int                   l = 0;
	Real                  x, y, z, porosity2;
	while (file >> x >> y >> z >> porosity2) {
		gridCoords.push_back(Vector3r(x, y, z));
		//gridCoords[l][1] = y;
		//gridCoords[l][2] = z;
		porosities.push_back(porosity2);
		l++;
	}
	cout << "finished creating coords vec and porosities" << endl;
	Tesselation& Tes = flow.T[flow.currentTes];

	const long size = Tes.cellHandles.size();
#pragma omp parallel for num_threads(ompThreads>0 ? ompThreads : 1)
	for (long i = 0; i < size; i++) {
		CellHandle& cell      = Tes.cellHandles[i];
		Real        finalDist = 1e10;
		Real        finalPoro = meanInitialPorosity;
		CVector     bc        = flow.cellBarycenter(cell);

		Vector3r cellCoords(bc[0], bc[1], bc[2]);
		if (!resetVolumeSolids) {
			for (unsigned int k = 0; k < gridCoords.size(); k++) {
				Vector3r vec = cellCoords - gridCoords[k];
				if (vec.norm() < finalDist) {
					finalDist = vec.norm();
					finalPoro = porosities[k];
				}
			}

			// finalPoro = meanInitialPorosity;
			if (finalPoro <= minPoroClamp)
				finalPoro = minPoroClamp;
			if (finalPoro >= maxPoroClamp)
				finalPoro = maxPoroClamp;

			cell->info().porosity = cell->info().initialPorosity = finalPoro;
			if (cell->info().Pcondition) {
				//cout << "setting boundary cell porosity to mean initial" << endl;
				cell->info().porosity = cell->info().initialPorosity = meanInitialPorosity;
			}
			if (finalPoro > maxPorosity)
				maxPorosity = finalPoro;
		}

		cell->info().vSolids = cell->info().vMixture * (1. - cell->info().porosity);
		if (!resetVolumeSolids) {
			cell->info().Po = Po* exp(a * (meanInitialPorosity - cell->info().porosity)); // use unique cell initial porosity or overall average porosity (mu)?
			cell->info().lambdao = lmbda * exp(b * (meanInitialPorosity - cell->info().porosity));
		}
	}
	if (resetVolumeSolids)
		resetVolumeSolids = false;
}

void TransportFlowEngine::averageRelativeCellVelocity_transport()
{
	//if (noCache && T[!currentTes].Max_id() <= 0) return;
	RTriangulation&     Tri = solver->T[solver->currentTes].Triangulation(); // T[noCache ? (!currentTes) : currentTes].Triangulation();
	Point               posAvFacet;
	int                 numCells = 0;
	Real                facetFlowRate = 0;
	FiniteCellsIterator cellEnd = Tri.finite_cells_end();
	for (FiniteCellsIterator cell = Tri.finite_cells_begin(); cell != cellEnd; cell++) {
		if (cell->info().isGhost or cell->info().isAlpha) continue;
		cell->info().averageVelocity() = CGAL::NULL_VECTOR;
		numCells++;
		Real totFlowRate = 0; //used to acount for influxes in elements where pressure is imposed
		for (int i = 0; i < 4; i++)
			if (!Tri.is_infinite(cell->neighbor(i))) {
				CVector Surfk = cell->info() - cell->neighbor(i)->info();
				Real    area = sqrt(Surfk.squared_length());
				Surfk = Surfk / area;
				CVector branch = cell->vertex(facetVertices[i][0])->point().point() - cell->info();
				posAvFacet = (Point)cell->info() + (branch * Surfk) * Surfk;
				facetFlowRate = (cell->info().kNorm())[i] * (cell->info().potential - cell->neighbor(i)->info().potential);
				totFlowRate += facetFlowRate;
				cell->info().averageVelocity() = cell->info().averageVelocity() + (facetFlowRate) * (posAvFacet - CGAL::ORIGIN);
			}
		//This is the influx term
		if (cell->info().Pcondition)
			cell->info().averageVelocity() = cell->info().averageVelocity() - (totFlowRate) * ((Point)cell->info() - CGAL::ORIGIN);
		//now divide by volume
		if (cell->info().volume() == 0) cerr << "zero volume pore interrupting velocity calculation" << endl;
		else
			cell->info().averageVelocity() = cell->info().averageVelocity() / math::abs(cell->info().volume());
	}
}





} // namespace yade

#endif //YADE_OPENMP
#endif //TRANSPORT
